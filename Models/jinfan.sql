/*
Navicat MySQL Data Transfer

Source Server         : ALY
Source Server Version : 50628
Source Host           : 57b40ac159168.bj.cdb.myqcloud.com:13992
Source Database       : jinfan

Target Server Type    : MYSQL
Target Server Version : 50628
File Encoding         : 65001

Date: 2017-05-03 16:10:41
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for `article`
-- ----------------------------
DROP TABLE IF EXISTS `article`;
CREATE TABLE `article` (
  `Id` int(11) NOT NULL AUTO_INCREMENT,
  `ArticleType_Id` int(11) NOT NULL,
  `Article_key` longtext,
  `Article_name` longtext,
  `Article_txt` longtext,
  `Createtime` datetime(6) NOT NULL,
  `Disable` int(11) NOT NULL,
  `Disabledesc` longtext,
  `User_Id` int(11) NOT NULL,
  `Admin_Createtime` datetime(6) NOT NULL DEFAULT '0001-01-01 00:00:00.000000',
  `Admin_Id` int(11) NOT NULL DEFAULT '0',
  `Admin_remark` longtext,
  `Admin_step` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`Id`),
  KEY `article_typeId` (`ArticleType_Id`),
  KEY `article_userid` (`User_Id`),
  KEY `article_adminid` (`Admin_Id`),
  CONSTRAINT `article_adminid` FOREIGN KEY (`Admin_Id`) REFERENCES `users` (`Id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `article_typeId` FOREIGN KEY (`ArticleType_Id`) REFERENCES `articletype` (`Id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `article_userid` FOREIGN KEY (`User_Id`) REFERENCES `users` (`Id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=26 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of article
-- ----------------------------
INSERT INTO `article` VALUES ('1', '1', 'netcore', '测试', '<div id=\"article_content\" class=\"article_content\">\r\n\r\n<p>EF的Code First方式允许你先写Model,再通过Model生成<a href=\"http://lib.csdn.net/base/mysql\" class=\"replace_word\" title=\"MySQL知识库\" target=\"_blank\" style=\"color:#df3434; font-weight:bold;\">数据库</a>和表。</p>\r\n<p>具体步骤如下：</p>\r\n<p>1、建项目</p>\r\n<p>2、在model文件夹中，添加一个派生自DbContext的类，和一些Model类。</p>\r\n<p>3、修改web.Config中的连接字符串。</p>\r\n<p>4、生成基架Controller。</p>\r\n<p>这样就可以生成数据库和表。</p>\r\n<p>但实际开发中难免会修改模型和派生的Context，修改后再次运行会出现异常，提示“<span style=\"color:#ff0000\">支持“MyContext”上下文的模型已在数据库创建后发生更改。请考虑使用 Code First 迁移更新数据库”</span><span style=\"color:#000000\">异常。</span></p>\r\n<p>可以通过如下步骤更新数据库，消除上述异常：</p>\r\n<p>1、在项目的“程序包管理控制台\"窗口，输入”enable-migrations\"，回车，导致在项目中生成一个“Migrations\"文件夹，其中包含两个.cs类。</p>\r\n<p>2、在1中生成的Configuration.cs中修改构造函数中代码为：”AutomaticMigrationsEnabled = true;“</p>\r\n<p>3、保存修改后，在“程序包管理控制台\"中输入”update-database\"，回车，再次运行即可。</p>\r\n   \r\n', '2017-03-21 15:36:25.953273', '0', null, '3', '0001-01-01 00:00:00.000000', '2', null, '1');
INSERT INTO `article` VALUES ('2', '2', null, '测试', '<p class=\"MsoNormal\" align=\"center\" style=\"text-align:center;\">\r\n	<span style=\"font-size:12pt;font-family:微软雅黑, sans-serif;\">U</span><span style=\"font-size:12pt;font-family:微软雅黑, sans-serif;\">盘启动安装说明<span></span></span>\r\n</p>\r\n<p class=\"MsoListParagraph\" style=\"margin-left:18.0pt;text-indent:-18.0pt;\">\r\n	<span style=\"font-size:12pt;font-family:微软雅黑, sans-serif;\">1.<span style=\"font-size:7pt;line-height:normal;font-family:&quot;\">&nbsp;&nbsp;&nbsp; </span></span><span style=\"font-size:12pt;font-family:微软雅黑, sans-serif;\">将<span>U</span>盘插入电脑后，根据电脑品牌或者主板品牌在</span><span style=\"font-size:12pt;font-family:微软雅黑, sans-serif;\">BIOS</span><span style=\"font-size:12pt;font-family:微软雅黑, sans-serif;\">设置</span><span style=\"font-size:12pt;font-family:微软雅黑, sans-serif;\">找到对应快捷键将<span>U</span>盘设置为第一启动盘，重启电脑后就会自动运行<span>PE</span>系统。</span><strong><span style=\"font-size:12.0pt;font-family:&quot;color:#E36C0A;\">重要提醒：选择热键前，请先插入<span>U</span>盘。</span></strong><span style=\"font-size:12pt;font-family:微软雅黑, sans-serif;\">对应<span>USB</span>模式启动热键如图：</span><b><span style=\"font-size:12.0pt;font-family:&quot;color:#303030;\"></span></b>\r\n</p>\r\n<p class=\"MsoNormal\" align=\"center\" style=\"text-align:center;\">\r\n	<span style=\"font-size:12.0pt;font-family:&quot;\"></span><span style=\"font-size:12.0pt;font-family:&quot;\"></span>\r\n</p>\r\n<p class=\"MsoListParagraph\" style=\"margin-left:18.0pt;text-indent:-18.0pt;\">\r\n	<span style=\"font-size:12pt;font-family:微软雅黑, sans-serif;\">2.<span style=\"font-size:7pt;line-height:normal;font-family:&quot;\">&nbsp;&nbsp;&nbsp; </span></span><span style=\"font-size:12.0pt;font-family:&quot;\">U</span><span style=\"font-size:12.0pt;font-family:&quot;\">盘启动后，按上下键选中菜单选项【<span>01</span>】后回车，如图：<span></span></span>\r\n</p>\r\n<p class=\"MsoNormal\" align=\"center\" style=\"text-align:center;\">\r\n	<span style=\"font-size:12.0pt;font-family:&quot;\"></span><span style=\"font-size:12.0pt;font-family:&quot;\"></span>\r\n</p>\r\n<p class=\"MsoNormal\" align=\"center\" style=\"text-align:center;\">\r\n	<span style=\"font-size:12.0pt;font-family:&quot;\"></span>\r\n</p>\r\n<p class=\"MsoListParagraph\" style=\"margin-left:18.0pt;text-indent:-18.0pt;\">\r\n	<span style=\"font-size:12pt;font-family:微软雅黑, sans-serif;\">3.<span style=\"font-size:7pt;line-height:normal;font-family:&quot;\">&nbsp;&nbsp;&nbsp; </span></span><span style=\"font-size:12pt;font-family:微软雅黑, sans-serif;color:#303030;\">进入<span>PE</span>后，打开桌面上的“一键还原备份系统”，再单击“浏览”按钮，选择<span>U</span>盘下的<span>iso</span>文件，然后单击“打开”按钮即可（</span><b><span style=\"font-size:12pt;font-family:微软雅黑, sans-serif;color:#E36C0A;\">注意：有<span>2</span>个<span>ios</span>，确认选择镜像是否准确</span></b><span style=\"font-size:12pt;font-family:微软雅黑, sans-serif;color:#303030;\">）。如图：<span></span></span>\r\n</p>\r\n<p class=\"MsoNormal\" align=\"center\" style=\"text-align:center;\">\r\n	<span style=\"font-size:12.0pt;font-family:&quot;\"></span><span style=\"font-size:12.0pt;font-family:&quot;\"></span>\r\n</p>\r\n<p class=\"MsoListParagraph\" style=\"margin-left:18.0pt;text-indent:-18.0pt;\">\r\n	<span style=\"font-size:12pt;font-family:微软雅黑, sans-serif;\">4.<span style=\"font-size:7pt;line-height:normal;font-family:&quot;\">&nbsp;&nbsp;&nbsp; </span></span><span style=\"font-size:12pt;font-family:微软雅黑, sans-serif;color:#303030;\">选择需要安装的系统分区（正常选择<span>C</span>盘），然后单击“开始”按钮。如图：<span></span></span>\r\n</p>\r\n<p class=\"MsoNormal\" align=\"center\" style=\"text-align:center;\">\r\n	<span style=\"font-size:12.0pt;font-family:&quot;\"></span><span style=\"font-size:12.0pt;font-family:&quot;\"></span>\r\n</p>\r\n<p class=\"MsoListParagraph\" style=\"margin-left:18.0pt;text-indent:-18.0pt;\">\r\n	<span style=\"font-size:12pt;font-family:微软雅黑, sans-serif;\">5.<span style=\"font-size:7pt;line-height:normal;font-family:&quot;\">&nbsp;&nbsp;&nbsp; </span></span><span style=\"font-size:12pt;font-family:微软雅黑, sans-serif;color:#303030;\">接下来系统会自动运行，如图：<span></span></span>\r\n</p>\r\n<p class=\"MsoNormal\" align=\"center\" style=\"text-align:center;\">\r\n	<span style=\"font-size:12.0pt;font-family:&quot;\"></span><span style=\"font-size:12.0pt;font-family:&quot;\"></span>\r\n</p>\r\n<p class=\"MsoListParagraph\" style=\"margin-left:18.0pt;text-indent:-18.0pt;\">\r\n	<span style=\"font-size:12pt;font-family:微软雅黑, sans-serif;\">6.<span style=\"font-size:7pt;line-height:normal;font-family:&quot;\">&nbsp;&nbsp;&nbsp; </span></span><span style=\"font-size:12.0pt;font-family:&quot;color:#303030;\">ghost</span><span style=\"font-size:12.0pt;font-family:&quot;color:#303030;\">系统安装完毕后，会弹出“还原成功”窗口提示电脑将在<span>15</span>秒后自动重启，如图：<span></span></span>\r\n</p>\r\n<p class=\"MsoNormal\" align=\"center\" style=\"text-align:center;\">\r\n	<span style=\"font-size:12.0pt;font-family:&quot;\"></span><span style=\"font-size:12.0pt;font-family:&quot;\"></span>\r\n</p>\r\n<p class=\"MsoListParagraph\" style=\"margin-left:18.0pt;text-indent:-18.0pt;\">\r\n	<span style=\"font-size:12pt;font-family:微软雅黑, sans-serif;\">7.<span style=\"font-size:7pt;line-height:normal;font-family:&quot;\">&nbsp;&nbsp;&nbsp; </span></span><span style=\"font-size:12pt;font-family:微软雅黑, sans-serif;color:#303030;\">等待电脑重启后，由于镜像文件包含的原硬件驱动可能不匹配，安装驱动精灵 万能网卡版（在<span>U</span>盘的根目录下，<span>DGSetup_1289E.exe</span>），安装完成后更新所有驱动。</span><strong><span style=\"font-size:12.0pt;font-family:&quot;color:#F79646;\">重要提醒：更新驱动需要网络下载。</span></strong><span style=\"font-size:12pt;font-family:微软雅黑, sans-serif;color:#303030;\"></span>\r\n</p>', '2017-03-21 14:03:53.670223', '1', null, '3', '2017-03-30 10:27:16.391842', '2', null, '2');
INSERT INTO `article` VALUES ('3', '3', '', '测试', '<p class=\"MsoNormal\" align=\"center\" style=\"text-align:center;\">\r\n	<span style=\"font-size:12pt;font-family:微软雅黑, sans-serif;\">eU</span><span style=\"font-size:12pt;font-family:微软雅黑, sans-serif;\">盘启动安装说明<span></span></span> \r\n</p>\r\n<p class=\"MsoListParagraph\" style=\"margin-left:18.0pt;text-indent:-18.0pt;\">\r\n	<span style=\"font-size:12pt;font-family:微软雅黑, sans-serif;\">1.<span style=\"font-size:7pt;line-height:normal;font-family:&quot;\">&nbsp;&nbsp;&nbsp; </span></span><span style=\"font-size:12pt;font-family:微软雅黑, sans-serif;\">将<span>U</span>盘插入电脑后，根据电脑品牌或者主板品牌在</span><span style=\"font-size:12pt;font-family:微软雅黑, sans-serif;\">BIOS</span><span style=\"font-size:12pt;font-family:微软雅黑, sans-serif;\">设置</span><span style=\"font-size:12pt;font-family:微软雅黑, sans-serif;\">找到对应快捷键将<span>U</span>盘设置为第一启动盘，重启电脑后就会自动运行<span>PE</span>系统。</span><strong><span style=\"font-size:12.0pt;font-family:&quot;color:#E36C0A;\">重要提醒：选择热键前，请先插入<span>U</span>盘。</span></strong><span style=\"font-size:12pt;font-family:微软雅黑, sans-serif;\">对应<span>USB</span>模式启动热键如图：</span><b><span style=\"font-size:12.0pt;font-family:&quot;color:#303030;\"></span></b> \r\n</p>\r\n<p class=\"MsoNormal\" align=\"center\" style=\"text-align:center;\">\r\n	<span style=\"font-size:12.0pt;font-family:&quot;\"></span><span style=\"font-size:12.0pt;font-family:&quot;\"></span> \r\n</p>\r\n<p class=\"MsoListParagraph\" style=\"margin-left:18.0pt;text-indent:-18.0pt;\">\r\n	<span style=\"font-size:12pt;font-family:微软雅黑, sans-serif;\">2.<span style=\"font-size:7pt;line-height:normal;font-family:&quot;\">&nbsp;&nbsp;&nbsp; </span></span><span style=\"font-size:12.0pt;font-family:&quot;\">U</span><span style=\"font-size:12.0pt;font-family:&quot;\">盘启动后，按上下键选中菜单选项【<span>01</span>】后回车，如图：<span></span></span> \r\n</p>\r\n<p class=\"MsoNormal\" align=\"center\" style=\"text-align:center;\">\r\n	<span style=\"font-size:12.0pt;font-family:&quot;\"></span><span style=\"font-size:12.0pt;font-family:&quot;\"></span> \r\n</p>\r\n<p class=\"MsoNormal\" align=\"center\" style=\"text-align:center;\">\r\n	<span style=\"font-size:12.0pt;font-family:&quot;\"></span> \r\n</p>\r\n<p class=\"MsoListParagraph\" style=\"margin-left:18.0pt;text-indent:-18.0pt;\">\r\n	<span style=\"font-size:12pt;font-family:微软雅黑, sans-serif;\">3.<span style=\"font-size:7pt;line-height:normal;font-family:&quot;\">&nbsp;&nbsp;&nbsp; </span></span><span style=\"font-size:12pt;font-family:微软雅黑, sans-serif;color:#303030;\">进入<span>PE</span>后，打开桌面上的“一键还原备份系统”，再单击“浏览”按钮，选择<span>U</span>盘下的<span>iso</span>文件，然后单击“打开”按钮即可（</span><b><span style=\"font-size:12pt;font-family:微软雅黑, sans-serif;color:#E36C0A;\">注意：有<span>2</span>个<span>ios</span>，确认选择镜像是否准确</span></b><span style=\"font-size:12pt;font-family:微软雅黑, sans-serif;color:#303030;\">）。如图：<span></span></span> \r\n</p>\r\n<p class=\"MsoNormal\" align=\"center\" style=\"text-align:center;\">\r\n	<span style=\"font-size:12.0pt;font-family:&quot;\"></span><span style=\"font-size:12.0pt;font-family:&quot;\"></span> \r\n</p>\r\n<p class=\"MsoListParagraph\" style=\"margin-left:18.0pt;text-indent:-18.0pt;\">\r\n	<span style=\"font-size:12pt;font-family:微软雅黑, sans-serif;\">4.<span style=\"font-size:7pt;line-height:normal;font-family:&quot;\">&nbsp;&nbsp;&nbsp; </span></span><span style=\"font-size:12pt;font-family:微软雅黑, sans-serif;color:#303030;\">选择需要安装的系统分区（正常选择<span>C</span>盘），然后单击“开始”按钮。如图：<span></span></span> \r\n</p>\r\n<p class=\"MsoNormal\" align=\"center\" style=\"text-align:center;\">\r\n	<span style=\"font-size:12.0pt;font-family:&quot;\"></span><span style=\"font-size:12.0pt;font-family:&quot;\"></span> \r\n</p>\r\n<p class=\"MsoListParagraph\" style=\"margin-left:18.0pt;text-indent:-18.0pt;\">\r\n	<span style=\"font-size:12pt;font-family:微软雅黑, sans-serif;\">5.<span style=\"font-size:7pt;line-height:normal;font-family:&quot;\">&nbsp;&nbsp;&nbsp; </span></span><span style=\"font-size:12pt;font-family:微软雅黑, sans-serif;color:#303030;\">接下来系统会自动运行，如图：<span></span></span> \r\n</p>\r\n<p class=\"MsoNormal\" align=\"center\" style=\"text-align:center;\">\r\n	<span style=\"font-size:12.0pt;font-family:&quot;\"></span><span style=\"font-size:12.0pt;font-family:&quot;\"></span> \r\n</p>\r\n<p class=\"MsoListParagraph\" style=\"margin-left:18.0pt;text-indent:-18.0pt;\">\r\n	<span style=\"font-size:12pt;font-family:微软雅黑, sans-serif;\">6.<span style=\"font-size:7pt;line-height:normal;font-family:&quot;\">&nbsp;&nbsp;&nbsp; </span></span><span style=\"font-size:12.0pt;font-family:&quot;color:#303030;\">ghost</span><span style=\"font-size:12.0pt;font-family:&quot;color:#303030;\">系统安装完毕后，会弹出“还原成功”窗口提示电脑将在<span>15</span>秒后自动重启，如图：<span></span></span> \r\n</p>\r\n<p class=\"MsoNormal\" align=\"center\" style=\"text-align:center;\">\r\n	<span style=\"font-size:12.0pt;font-family:&quot;\"></span><span style=\"font-size:12.0pt;font-family:&quot;\"></span> \r\n</p>\r\n<p class=\"MsoListParagraph\" style=\"margin-left:18.0pt;text-indent:-18.0pt;\">\r\n	<span style=\"font-size:12pt;font-family:微软雅黑, sans-serif;\">7.<span style=\"font-size:7pt;line-height:normal;font-family:&quot;\">&nbsp;&nbsp;&nbsp; </span></span><span style=\"font-size:12pt;font-family:微软雅黑, sans-serif;color:#303030;\">等待电脑重启后，由于镜像文件包含的原硬件驱动可能不匹配，安装驱动精灵 万能网卡版（在<span>U</span>盘的根目录下，<span>DGSetup_1289E.exe</span>），安装完成后更新所有驱动。</span><strong><span style=\"font-size:12.0pt;font-family:&quot;color:#F79646;\">重要提醒：更新驱动需要网络下载。</span></strong><span style=\"font-size:12pt;font-family:微软雅黑, sans-serif;color:#303030;\"></span> \r\n</p>', '2017-03-21 15:10:49.000000', '1', null, '3', '0001-01-01 00:00:00.000000', '2', null, '6');
INSERT INTO `article` VALUES ('4', '1', '', '测试', '<p class=\"MsoNormal\" align=\"center\" style=\"text-align:center;\">\r\n	<span style=\"font-size:12pt;font-family:微软雅黑, sans-serif;\">U</span><span style=\"font-size:12pt;font-family:微软雅黑, sans-serif;\">盘启动安装说明<span></span></span>\r\n</p>\r\n<p class=\"MsoListParagraph\" style=\"margin-left:18.0pt;text-indent:-18.0pt;\">\r\n	<span style=\"font-size:12pt;font-family:微软雅黑, sans-serif;\">1.<span style=\"font-size:7pt;line-height:normal;font-family:&quot;\">&nbsp;&nbsp;&nbsp; </span></span><span style=\"font-size:12pt;font-family:微软雅黑, sans-serif;\">将<span>U</span>盘插入电脑后，根据电脑品牌或者主板品牌在</span><span style=\"font-size:12pt;font-family:微软雅黑, sans-serif;\">BIOS</span><span style=\"font-size:12pt;font-family:微软雅黑, sans-serif;\">设置</span><span style=\"font-size:12pt;font-family:微软雅黑, sans-serif;\">找到对应快捷键将<span>U</span>盘设置为第一启动盘，重启电脑后就会自动运行<span>PE</span>系统。</span><strong><span style=\"font-size:12.0pt;font-family:&quot;color:#E36C0A;\">重要提醒：选择热键前，请先插入<span>U</span>盘。</span></strong><span style=\"font-size:12pt;font-family:微软雅黑, sans-serif;\">对应<span>USB</span>模式启动热键如图：</span><b><span style=\"font-size:12.0pt;font-family:&quot;color:#303030;\"></span></b>\r\n</p>\r\n<p class=\"MsoNormal\" align=\"center\" style=\"text-align:center;\">\r\n	<span style=\"font-size:12.0pt;font-family:&quot;\"></span><span style=\"font-size:12.0pt;font-family:&quot;\"></span>\r\n</p>\r\n<p class=\"MsoListParagraph\" style=\"margin-left:18.0pt;text-indent:-18.0pt;\">\r\n	<span style=\"font-size:12pt;font-family:微软雅黑, sans-serif;\">2.<span style=\"font-size:7pt;line-height:normal;font-family:&quot;\">&nbsp;&nbsp;&nbsp; </span></span><span style=\"font-size:12.0pt;font-family:&quot;\">U</span><span style=\"font-size:12.0pt;font-family:&quot;\">盘启动后，按上下键选中菜单选项【<span>01</span>】后回车，如图：<span></span></span>\r\n</p>\r\n<p class=\"MsoNormal\" align=\"center\" style=\"text-align:center;\">\r\n	<span style=\"font-size:12.0pt;font-family:&quot;\"></span><span style=\"font-size:12.0pt;font-family:&quot;\"></span>\r\n</p>\r\n<p class=\"MsoNormal\" align=\"center\" style=\"text-align:center;\">\r\n	<span style=\"font-size:12.0pt;font-family:&quot;\"></span>\r\n</p>\r\n<p class=\"MsoListParagraph\" style=\"margin-left:18.0pt;text-indent:-18.0pt;\">\r\n	<span style=\"font-size:12pt;font-family:微软雅黑, sans-serif;\">3.<span style=\"font-size:7pt;line-height:normal;font-family:&quot;\">&nbsp;&nbsp;&nbsp; </span></span><span style=\"font-size:12pt;font-family:微软雅黑, sans-serif;color:#303030;\">进入<span>PE</span>后，打开桌面上的“一键还原备份系统”，再单击“浏览”按钮，选择<span>U</span>盘下的<span>iso</span>文件，然后单击“打开”按钮即可（</span><b><span style=\"font-size:12pt;font-family:微软雅黑, sans-serif;color:#E36C0A;\">注意：有<span>2</span>个<span>ios</span>，确认选择镜像是否准确</span></b><span style=\"font-size:12pt;font-family:微软雅黑, sans-serif;color:#303030;\">）。如图：<span></span></span>\r\n</p>\r\n<p class=\"MsoNormal\" align=\"center\" style=\"text-align:center;\">\r\n	<span style=\"font-size:12.0pt;font-family:&quot;\"></span><span style=\"font-size:12.0pt;font-family:&quot;\"></span>\r\n</p>\r\n<p class=\"MsoListParagraph\" style=\"margin-left:18.0pt;text-indent:-18.0pt;\">\r\n	<span style=\"font-size:12pt;font-family:微软雅黑, sans-serif;\">4.<span style=\"font-size:7pt;line-height:normal;font-family:&quot;\">&nbsp;&nbsp;&nbsp; </span></span><span style=\"font-size:12pt;font-family:微软雅黑, sans-serif;color:#303030;\">选择需要安装的系统分区（正常选择<span>C</span>盘），然后单击“开始”按钮。如图：<span></span></span>\r\n</p>\r\n<p class=\"MsoNormal\" align=\"center\" style=\"text-align:center;\">\r\n	<span style=\"font-size:12.0pt;font-family:&quot;\"></span><span style=\"font-size:12.0pt;font-family:&quot;\"></span>\r\n</p>\r\n<p class=\"MsoListParagraph\" style=\"margin-left:18.0pt;text-indent:-18.0pt;\">\r\n	<span style=\"font-size:12pt;font-family:微软雅黑, sans-serif;\">5.<span style=\"font-size:7pt;line-height:normal;font-family:&quot;\">&nbsp;&nbsp;&nbsp; </span></span><span style=\"font-size:12pt;font-family:微软雅黑, sans-serif;color:#303030;\">接下来系统会自动运行，如图：<span></span></span>\r\n</p>\r\n<p class=\"MsoNormal\" align=\"center\" style=\"text-align:center;\">\r\n	<span style=\"font-size:12.0pt;font-family:&quot;\"></span><span style=\"font-size:12.0pt;font-family:&quot;\"></span>\r\n</p>\r\n<p class=\"MsoListParagraph\" style=\"margin-left:18.0pt;text-indent:-18.0pt;\">\r\n	<span style=\"font-size:12pt;font-family:微软雅黑, sans-serif;\">6.<span style=\"font-size:7pt;line-height:normal;font-family:&quot;\">&nbsp;&nbsp;&nbsp; </span></span><span style=\"font-size:12.0pt;font-family:&quot;color:#303030;\">ghost</span><span style=\"font-size:12.0pt;font-family:&quot;color:#303030;\">系统安装完毕后，会弹出“还原成功”窗口提示电脑将在<span>15</span>秒后自动重启，如图：<span></span></span>\r\n</p>\r\n<p class=\"MsoNormal\" align=\"center\" style=\"text-align:center;\">\r\n	<span style=\"font-size:12.0pt;font-family:&quot;\"></span><span style=\"font-size:12.0pt;font-family:&quot;\"></span>\r\n</p>\r\n<p class=\"MsoListParagraph\" style=\"margin-left:18.0pt;text-indent:-18.0pt;\">\r\n	<span style=\"font-size:12pt;font-family:微软雅黑, sans-serif;\">7.<span style=\"font-size:7pt;line-height:normal;font-family:&quot;\">&nbsp;&nbsp;&nbsp; </span></span><span style=\"font-size:12pt;font-family:微软雅黑, sans-serif;color:#303030;\">等待电脑重启后，由于镜像文件包含的原硬件驱动可能不匹配，安装驱动精灵 万能网卡版（在<span>U</span>盘的根目录下，<span>DGSetup_1289E.exe</span>），安装完成后更新所有驱动。</span><strong><span style=\"font-size:12.0pt;font-family:&quot;color:#F79646;\">重要提醒：更新驱动需要网络下载。</span></strong><span style=\"font-size:12pt;font-family:微软雅黑, sans-serif;color:#303030;\"></span>\r\n</p>', '2017-03-30 10:27:40.830262', '1', null, '2', '2017-03-30 10:55:51.730160', '2', null, '2');
INSERT INTO `article` VALUES ('5', '1', '', '测试', '<p class=\"MsoNormal\" align=\"center\" style=\"text-align:center;\">\r\n	<span style=\"font-size:12pt;font-family:微软雅黑, sans-serif;\">U</span><span style=\"font-size:12pt;font-family:微软雅黑, sans-serif;\">盘启动安装说明<span></span></span>\r\n</p>\r\n<p class=\"MsoListParagraph\" style=\"margin-left:18.0pt;text-indent:-18.0pt;\">\r\n	<span style=\"font-size:12pt;font-family:微软雅黑, sans-serif;\">1.<span style=\"font-size:7pt;line-height:normal;font-family:&quot;\">&nbsp;&nbsp;&nbsp; </span></span><span style=\"font-size:12pt;font-family:微软雅黑, sans-serif;\">将<span>U</span>盘插入电脑后，根据电脑品牌或者主板品牌在</span><span style=\"font-size:12pt;font-family:微软雅黑, sans-serif;\">BIOS</span><span style=\"font-size:12pt;font-family:微软雅黑, sans-serif;\">设置</span><span style=\"font-size:12pt;font-family:微软雅黑, sans-serif;\">找到对应快捷键将<span>U</span>盘设置为第一启动盘，重启电脑后就会自动运行<span>PE</span>系统。</span><strong><span style=\"font-size:12.0pt;font-family:&quot;color:#E36C0A;\">重要提醒：选择热键前，请先插入<span>U</span>盘。</span></strong><span style=\"font-size:12pt;font-family:微软雅黑, sans-serif;\">对应<span>USB</span>模式启动热键如图：</span><b><span style=\"font-size:12.0pt;font-family:&quot;color:#303030;\"></span></b>\r\n</p>\r\n<p class=\"MsoNormal\" align=\"center\" style=\"text-align:center;\">\r\n	<span style=\"font-size:12.0pt;font-family:&quot;\"></span><span style=\"font-size:12.0pt;font-family:&quot;\"></span>\r\n</p>\r\n<p class=\"MsoListParagraph\" style=\"margin-left:18.0pt;text-indent:-18.0pt;\">\r\n	<span style=\"font-size:12pt;font-family:微软雅黑, sans-serif;\">2.<span style=\"font-size:7pt;line-height:normal;font-family:&quot;\">&nbsp;&nbsp;&nbsp; </span></span><span style=\"font-size:12.0pt;font-family:&quot;\">U</span><span style=\"font-size:12.0pt;font-family:&quot;\">盘启动后，按上下键选中菜单选项【<span>01</span>】后回车，如图：<span></span></span>\r\n</p>\r\n<p class=\"MsoNormal\" align=\"center\" style=\"text-align:center;\">\r\n	<span style=\"font-size:12.0pt;font-family:&quot;\"></span><span style=\"font-size:12.0pt;font-family:&quot;\"></span>\r\n</p>\r\n<p class=\"MsoNormal\" align=\"center\" style=\"text-align:center;\">\r\n	<span style=\"font-size:12.0pt;font-family:&quot;\"></span>\r\n</p>\r\n<p class=\"MsoListParagraph\" style=\"margin-left:18.0pt;text-indent:-18.0pt;\">\r\n	<span style=\"font-size:12pt;font-family:微软雅黑, sans-serif;\">3.<span style=\"font-size:7pt;line-height:normal;font-family:&quot;\">&nbsp;&nbsp;&nbsp; </span></span><span style=\"font-size:12pt;font-family:微软雅黑, sans-serif;color:#303030;\">进入<span>PE</span>后，打开桌面上的“一键还原备份系统”，再单击“浏览”按钮，选择<span>U</span>盘下的<span>iso</span>文件，然后单击“打开”按钮即可（</span><b><span style=\"font-size:12pt;font-family:微软雅黑, sans-serif;color:#E36C0A;\">注意：有<span>2</span>个<span>ios</span>，确认选择镜像是否准确</span></b><span style=\"font-size:12pt;font-family:微软雅黑, sans-serif;color:#303030;\">）。如图：<span></span></span>\r\n</p>\r\n<p class=\"MsoNormal\" align=\"center\" style=\"text-align:center;\">\r\n	<span style=\"font-size:12.0pt;font-family:&quot;\"></span><span style=\"font-size:12.0pt;font-family:&quot;\"></span>\r\n</p>\r\n<p class=\"MsoListParagraph\" style=\"margin-left:18.0pt;text-indent:-18.0pt;\">\r\n	<span style=\"font-size:12pt;font-family:微软雅黑, sans-serif;\">4.<span style=\"font-size:7pt;line-height:normal;font-family:&quot;\">&nbsp;&nbsp;&nbsp; </span></span><span style=\"font-size:12pt;font-family:微软雅黑, sans-serif;color:#303030;\">选择需要安装的系统分区（正常选择<span>C</span>盘），然后单击“开始”按钮。如图：<span></span></span>\r\n</p>\r\n<p class=\"MsoNormal\" align=\"center\" style=\"text-align:center;\">\r\n	<span style=\"font-size:12.0pt;font-family:&quot;\"></span><span style=\"font-size:12.0pt;font-family:&quot;\"></span>\r\n</p>\r\n<p class=\"MsoListParagraph\" style=\"margin-left:18.0pt;text-indent:-18.0pt;\">\r\n	<span style=\"font-size:12pt;font-family:微软雅黑, sans-serif;\">5.<span style=\"font-size:7pt;line-height:normal;font-family:&quot;\">&nbsp;&nbsp;&nbsp; </span></span><span style=\"font-size:12pt;font-family:微软雅黑, sans-serif;color:#303030;\">接下来系统会自动运行，如图：<span></span></span>\r\n</p>\r\n<p class=\"MsoNormal\" align=\"center\" style=\"text-align:center;\">\r\n	<span style=\"font-size:12.0pt;font-family:&quot;\"></span><span style=\"font-size:12.0pt;font-family:&quot;\"></span>\r\n</p>\r\n<p class=\"MsoListParagraph\" style=\"margin-left:18.0pt;text-indent:-18.0pt;\">\r\n	<span style=\"font-size:12pt;font-family:微软雅黑, sans-serif;\">6.<span style=\"font-size:7pt;line-height:normal;font-family:&quot;\">&nbsp;&nbsp;&nbsp; </span></span><span style=\"font-size:12.0pt;font-family:&quot;color:#303030;\">ghost</span><span style=\"font-size:12.0pt;font-family:&quot;color:#303030;\">系统安装完毕后，会弹出“还原成功”窗口提示电脑将在<span>15</span>秒后自动重启，如图：<span></span></span>\r\n</p>\r\n<p class=\"MsoNormal\" align=\"center\" style=\"text-align:center;\">\r\n	<span style=\"font-size:12.0pt;font-family:&quot;\"></span><span style=\"font-size:12.0pt;font-family:&quot;\"></span>\r\n</p>\r\n<p class=\"MsoListParagraph\" style=\"margin-left:18.0pt;text-indent:-18.0pt;\">\r\n	<span style=\"font-size:12pt;font-family:微软雅黑, sans-serif;\">7.<span style=\"font-size:7pt;line-height:normal;font-family:&quot;\">&nbsp;&nbsp;&nbsp; </span></span><span style=\"font-size:12pt;font-family:微软雅黑, sans-serif;color:#303030;\">等待电脑重启后，由于镜像文件包含的原硬件驱动可能不匹配，安装驱动精灵 万能网卡版（在<span>U</span>盘的根目录下，<span>DGSetup_1289E.exe</span>），安装完成后更新所有驱动。</span><strong><span style=\"font-size:12.0pt;font-family:&quot;color:#F79646;\">重要提醒：更新驱动需要网络下载。</span></strong><span style=\"font-size:12pt;font-family:微软雅黑, sans-serif;color:#303030;\"></span>\r\n</p>', '2017-03-21 17:11:07.000000', '1', null, '2', '2017-03-30 10:27:13.055186', '2', null, '2');
INSERT INTO `article` VALUES ('6', '1', '', '测试', '<p class=\"MsoNormal\" align=\"center\" style=\"text-align:center;\">\r\n	<span style=\"font-size:12pt;font-family:微软雅黑, sans-serif;\">U</span><span style=\"font-size:12pt;font-family:微软雅黑, sans-serif;\">盘启动安装说明<span></span></span>\r\n</p>\r\n<p class=\"MsoListParagraph\" style=\"margin-left:18.0pt;text-indent:-18.0pt;\">\r\n	<span style=\"font-size:12pt;font-family:微软雅黑, sans-serif;\">1.<span style=\"font-size:7pt;line-height:normal;font-family:&quot;\">&nbsp;&nbsp;&nbsp; </span></span><span style=\"font-size:12pt;font-family:微软雅黑, sans-serif;\">将<span>U</span>盘插入电脑后，根据电脑品牌或者主板品牌在</span><span style=\"font-size:12pt;font-family:微软雅黑, sans-serif;\">BIOS</span><span style=\"font-size:12pt;font-family:微软雅黑, sans-serif;\">设置</span><span style=\"font-size:12pt;font-family:微软雅黑, sans-serif;\">找到对应快捷键将<span>U</span>盘设置为第一启动盘，重启电脑后就会自动运行<span>PE</span>系统。</span><strong><span style=\"font-size:12.0pt;font-family:&quot;color:#E36C0A;\">重要提醒：选择热键前，请先插入<span>U</span>盘。</span></strong><span style=\"font-size:12pt;font-family:微软雅黑, sans-serif;\">对应<span>USB</span>模式启动热键如图：</span><b><span style=\"font-size:12.0pt;font-family:&quot;color:#303030;\"></span></b>\r\n</p>\r\n<p class=\"MsoNormal\" align=\"center\" style=\"text-align:center;\">\r\n	<span style=\"font-size:12.0pt;font-family:&quot;\"></span><span style=\"font-size:12.0pt;font-family:&quot;\"></span>\r\n</p>\r\n<p class=\"MsoListParagraph\" style=\"margin-left:18.0pt;text-indent:-18.0pt;\">\r\n	<span style=\"font-size:12pt;font-family:微软雅黑, sans-serif;\">2.<span style=\"font-size:7pt;line-height:normal;font-family:&quot;\">&nbsp;&nbsp;&nbsp; </span></span><span style=\"font-size:12.0pt;font-family:&quot;\">U</span><span style=\"font-size:12.0pt;font-family:&quot;\">盘启动后，按上下键选中菜单选项【<span>01</span>】后回车，如图：<span></span></span>\r\n</p>\r\n<p class=\"MsoNormal\" align=\"center\" style=\"text-align:center;\">\r\n	<span style=\"font-size:12.0pt;font-family:&quot;\"></span><span style=\"font-size:12.0pt;font-family:&quot;\"></span>\r\n</p>\r\n<p class=\"MsoNormal\" align=\"center\" style=\"text-align:center;\">\r\n	<span style=\"font-size:12.0pt;font-family:&quot;\"></span>\r\n</p>\r\n<p class=\"MsoListParagraph\" style=\"margin-left:18.0pt;text-indent:-18.0pt;\">\r\n	<span style=\"font-size:12pt;font-family:微软雅黑, sans-serif;\">3.<span style=\"font-size:7pt;line-height:normal;font-family:&quot;\">&nbsp;&nbsp;&nbsp; </span></span><span style=\"font-size:12pt;font-family:微软雅黑, sans-serif;color:#303030;\">进入<span>PE</span>后，打开桌面上的“一键还原备份系统”，再单击“浏览”按钮，选择<span>U</span>盘下的<span>iso</span>文件，然后单击“打开”按钮即可（</span><b><span style=\"font-size:12pt;font-family:微软雅黑, sans-serif;color:#E36C0A;\">注意：有<span>2</span>个<span>ios</span>，确认选择镜像是否准确</span></b><span style=\"font-size:12pt;font-family:微软雅黑, sans-serif;color:#303030;\">）。如图：<span></span></span>\r\n</p>\r\n<p class=\"MsoNormal\" align=\"center\" style=\"text-align:center;\">\r\n	<span style=\"font-size:12.0pt;font-family:&quot;\"></span><span style=\"font-size:12.0pt;font-family:&quot;\"></span>\r\n</p>\r\n<p class=\"MsoListParagraph\" style=\"margin-left:18.0pt;text-indent:-18.0pt;\">\r\n	<span style=\"font-size:12pt;font-family:微软雅黑, sans-serif;\">4.<span style=\"font-size:7pt;line-height:normal;font-family:&quot;\">&nbsp;&nbsp;&nbsp; </span></span><span style=\"font-size:12pt;font-family:微软雅黑, sans-serif;color:#303030;\">选择需要安装的系统分区（正常选择<span>C</span>盘），然后单击“开始”按钮。如图：<span></span></span>\r\n</p>\r\n<p class=\"MsoNormal\" align=\"center\" style=\"text-align:center;\">\r\n	<span style=\"font-size:12.0pt;font-family:&quot;\"></span><span style=\"font-size:12.0pt;font-family:&quot;\"></span>\r\n</p>\r\n<p class=\"MsoListParagraph\" style=\"margin-left:18.0pt;text-indent:-18.0pt;\">\r\n	<span style=\"font-size:12pt;font-family:微软雅黑, sans-serif;\">5.<span style=\"font-size:7pt;line-height:normal;font-family:&quot;\">&nbsp;&nbsp;&nbsp; </span></span><span style=\"font-size:12pt;font-family:微软雅黑, sans-serif;color:#303030;\">接下来系统会自动运行，如图：<span></span></span>\r\n</p>\r\n<p class=\"MsoNormal\" align=\"center\" style=\"text-align:center;\">\r\n	<span style=\"font-size:12.0pt;font-family:&quot;\"></span><span style=\"font-size:12.0pt;font-family:&quot;\"></span>\r\n</p>\r\n<p class=\"MsoListParagraph\" style=\"margin-left:18.0pt;text-indent:-18.0pt;\">\r\n	<span style=\"font-size:12pt;font-family:微软雅黑, sans-serif;\">6.<span style=\"font-size:7pt;line-height:normal;font-family:&quot;\">&nbsp;&nbsp;&nbsp; </span></span><span style=\"font-size:12.0pt;font-family:&quot;color:#303030;\">ghost</span><span style=\"font-size:12.0pt;font-family:&quot;color:#303030;\">系统安装完毕后，会弹出“还原成功”窗口提示电脑将在<span>15</span>秒后自动重启，如图：<span></span></span>\r\n</p>\r\n<p class=\"MsoNormal\" align=\"center\" style=\"text-align:center;\">\r\n	<span style=\"font-size:12.0pt;font-family:&quot;\"></span><span style=\"font-size:12.0pt;font-family:&quot;\"></span>\r\n</p>\r\n<p class=\"MsoListParagraph\" style=\"margin-left:18.0pt;text-indent:-18.0pt;\">\r\n	<span style=\"font-size:12pt;font-family:微软雅黑, sans-serif;\">7.<span style=\"font-size:7pt;line-height:normal;font-family:&quot;\">&nbsp;&nbsp;&nbsp; </span></span><span style=\"font-size:12pt;font-family:微软雅黑, sans-serif;color:#303030;\">等待电脑重启后，由于镜像文件包含的原硬件驱动可能不匹配，安装驱动精灵 万能网卡版（在<span>U</span>盘的根目录下，<span>DGSetup_1289E.exe</span>），安装完成后更新所有驱动。</span><strong><span style=\"font-size:12.0pt;font-family:&quot;color:#F79646;\">重要提醒：更新驱动需要网络下载。</span></strong><span style=\"font-size:12pt;font-family:微软雅黑, sans-serif;color:#303030;\"></span>\r\n</p>', '2017-03-30 10:46:18.870582', '1', null, '2', '2017-03-30 10:46:18.870582', '2', '4', '4');
INSERT INTO `article` VALUES ('7', '4', null, 'U盘启动安装说明', '<p class=\"MsoNormal\" align=\"center\" style=\"text-align:left;\">\r\n	<span style=\"font-size:12pt;font-family:微软雅黑, sans-serif;\">1.<span style=\"font-size:7pt;line-height:normal;font-family:&quot;\">&nbsp;&nbsp;&nbsp; </span></span><span style=\"font-size:12pt;font-family:微软雅黑, sans-serif;\">将U盘插入电脑后，根据电脑品牌或者主板品牌在</span><span style=\"font-size:12pt;font-family:微软雅黑, sans-serif;\">BIOS</span><span style=\"font-size:12pt;font-family:微软雅黑, sans-serif;\">设置</span><span style=\"font-size:12pt;font-family:微软雅黑, sans-serif;\">找到对应快捷键将U盘设置为第一启动盘，重启电脑后就会自动运行PE系统。</span><strong><span style=\"font-size:12.0pt;font-family:&quot;color:#E36C0A;\">重要提醒：选择热键前，请先插入U盘。</span></strong><span style=\"font-size:12pt;font-family:微软雅黑, sans-serif;\">对应USB模式启动热键如图：</span> \r\n</p>\r\n<p class=\"MsoNormal\" align=\"center\" style=\"text-align:center;\">\r\n	<span style=\"font-size:12pt;font-family:微软雅黑, sans-serif;\"><img src=\"/KEupload/753ba71e-4174-4e6b-9d85-3deabc9fc40a.png\" alt=\"\" /><br />\r\n</span>\r\n</p>\r\n<p class=\"MsoNormal\" align=\"center\" style=\"text-align:center;\">\r\n	<span style=\"font-size:12.0pt;font-family:&quot;\"></span><span style=\"font-size:12.0pt;font-family:&quot;\"></span> \r\n</p>\r\n<p class=\"MsoListParagraph\" style=\"margin-left:18.0pt;text-indent:-18.0pt;\">\r\n	<span style=\"font-size:12pt;font-family:微软雅黑, sans-serif;\">2.<span style=\"font-size:7pt;line-height:normal;font-family:&quot;\">&nbsp;&nbsp;&nbsp; </span></span><span style=\"font-size:12.0pt;font-family:&quot;\">U</span><span style=\"font-size:12.0pt;font-family:&quot;\">盘启动后，按上下键选中菜单选项【<span>01</span>】后回车，如图：<span></span></span> \r\n</p>\r\n<p class=\"MsoListParagraph\" style=\"text-align:center;margin-left:18pt;text-indent:-18pt;\">\r\n	<span style=\"font-size:12.0pt;font-family:&quot;\"><img src=\"/KEupload/c0c85ffd-dccb-4d21-bd8e-11a982a501e3.png\" alt=\"\" /><br />\r\n</span>\r\n</p>\r\n<p class=\"MsoNormal\" align=\"center\" style=\"text-align:center;\">\r\n	<span style=\"font-size:12.0pt;font-family:&quot;\"></span><span style=\"font-size:12.0pt;font-family:&quot;\"></span> \r\n</p>\r\n<p class=\"MsoNormal\" align=\"center\" style=\"text-align:center;\">\r\n	<span style=\"font-size:12.0pt;font-family:&quot;\"></span> \r\n</p>\r\n<p class=\"MsoListParagraph\" style=\"margin-left:18.0pt;text-indent:-18.0pt;\">\r\n	<span style=\"font-size:12pt;font-family:微软雅黑, sans-serif;\">3.<span style=\"font-size:7pt;line-height:normal;font-family:&quot;\">&nbsp;&nbsp;&nbsp; </span></span><span style=\"font-size:12pt;font-family:微软雅黑, sans-serif;color:#303030;\">进入<span>PE</span>后，打开桌面上的“一键还原备份系统”，再单击“浏览”按钮，选择<span>U</span>盘下的<span>iso</span>文件，然后单击“打开”按钮即可（</span><b><span style=\"font-size:12pt;font-family:微软雅黑, sans-serif;color:#E36C0A;\">注意：有<span>2</span>个<span>ios</span>，确认选择镜像是否准确</span></b><span style=\"font-size:12pt;font-family:微软雅黑, sans-serif;color:#303030;\">）。如图：<span></span></span> \r\n</p>\r\n<p class=\"MsoListParagraph\" style=\"text-align:center;margin-left:18pt;text-indent:-18pt;\">\r\n	<span style=\"font-size:12pt;font-family:微软雅黑, sans-serif;color:#303030;\"><img src=\"/KEupload/158c6488-236d-4b19-a3d2-fae706e8a1c9.png\" alt=\"\" /><br />\r\n</span>\r\n</p>\r\n<p class=\"MsoNormal\" align=\"center\" style=\"text-align:center;\">\r\n	<span style=\"font-size:12.0pt;font-family:&quot;\"></span><span style=\"font-size:12.0pt;font-family:&quot;\"></span> \r\n</p>\r\n<p class=\"MsoListParagraph\" style=\"margin-left:18.0pt;text-indent:-18.0pt;\">\r\n	<span style=\"font-size:12pt;font-family:微软雅黑, sans-serif;\">4.<span style=\"font-size:7pt;line-height:normal;font-family:&quot;\">&nbsp;&nbsp;&nbsp; </span></span><span style=\"font-size:12pt;font-family:微软雅黑, sans-serif;color:#303030;\">选择需要安装的系统分区（正常选择<span>C</span>盘），然后单击“开始”按钮。如图：<span></span></span> \r\n</p>\r\n<p class=\"MsoListParagraph\" style=\"text-align:center;margin-left:18pt;text-indent:-18pt;\">\r\n	<span style=\"font-size:12pt;font-family:微软雅黑, sans-serif;color:#303030;\"><img src=\"/KEupload/2a79c715-2435-4fb9-859a-22702e2dff56.png\" alt=\"\" /><br />\r\n</span>\r\n</p>\r\n<p class=\"MsoNormal\" align=\"center\" style=\"text-align:center;\">\r\n	<span style=\"font-size:12.0pt;font-family:&quot;\"></span><span style=\"font-size:12.0pt;font-family:&quot;\"></span> \r\n</p>\r\n<p class=\"MsoListParagraph\" style=\"margin-left:18.0pt;text-indent:-18.0pt;\">\r\n	<span style=\"font-size:12pt;font-family:微软雅黑, sans-serif;\">5.<span style=\"font-size:7pt;line-height:normal;font-family:&quot;\">&nbsp;&nbsp;&nbsp; </span></span><span style=\"font-size:12pt;font-family:微软雅黑, sans-serif;color:#303030;\">接下来系统会自动运行，如图：<span></span></span> \r\n</p>\r\n<p class=\"MsoListParagraph\" style=\"text-align:center;margin-left:18pt;text-indent:-18pt;\">\r\n	<span style=\"font-size:12pt;font-family:微软雅黑, sans-serif;color:#303030;\"><img src=\"/KEupload/ef433b00-cbd2-47e2-9f75-e78160c1c45c.jpg\" alt=\"\" /><br />\r\n</span>\r\n</p>\r\n<p class=\"MsoNormal\" align=\"center\" style=\"text-align:center;\">\r\n	<span style=\"font-size:12.0pt;font-family:&quot;\"></span><span style=\"font-size:12.0pt;font-family:&quot;\"></span> \r\n</p>\r\n<p class=\"MsoListParagraph\" style=\"margin-left:18.0pt;text-indent:-18.0pt;\">\r\n	<span style=\"font-size:12pt;font-family:微软雅黑, sans-serif;\">6.<span style=\"font-size:7pt;line-height:normal;font-family:&quot;\">&nbsp;&nbsp;&nbsp; </span></span><span style=\"font-size:12.0pt;font-family:&quot;color:#303030;\">ghost</span><span style=\"font-size:12.0pt;font-family:&quot;color:#303030;\">系统安装完毕后，会弹出“还原成功”窗口提示电脑将在<span>15</span>秒后自动重启，如图：</span>\r\n</p>\r\n<p class=\"MsoListParagraph\" style=\"text-align:center;margin-left:18pt;text-indent:-18pt;\">\r\n	<span style=\"font-size:12.0pt;font-family:&quot;color:#303030;\"><img src=\"/KEupload/f1fec9cc-b1c1-4678-99ae-06a3777c729b.png\" alt=\"\" /><br />\r\n</span>\r\n</p>\r\n<p class=\"MsoNormal\" align=\"center\" style=\"text-align:center;\">\r\n	<span style=\"font-size:12.0pt;font-family:&quot;\"></span><span style=\"font-size:12.0pt;font-family:&quot;\"></span> \r\n</p>\r\n<p class=\"MsoListParagraph\" style=\"margin-left:18.0pt;text-indent:-18.0pt;\">\r\n	<span style=\"font-size:12pt;font-family:微软雅黑, sans-serif;\">7.<span style=\"font-size:7pt;line-height:normal;font-family:&quot;\">&nbsp;&nbsp;&nbsp; </span></span><span style=\"font-size:12pt;font-family:微软雅黑, sans-serif;color:#303030;\">等待电脑重启后，由于镜像文件包含的原硬件驱动可能不匹配，安装驱动精灵 万能网卡版（在<span>U</span>盘的根目录下，<span>DGSetup_1289E.exe</span>），安装完成后更新所有驱动。</span><strong><span style=\"font-size:12.0pt;font-family:&quot;color:#F79646;\">重要提醒：更新驱动需要网络下载。</span></strong><span style=\"font-size:12pt;font-family:微软雅黑, sans-serif;color:#303030;\"></span> \r\n</p>\r\n<p class=\"MsoListParagraph\" style=\"margin-left:18.0pt;text-indent:-18.0pt;\">\r\n	<span style=\"font-size:16px;\"><b>或者使用离线版万能驱动（W10——64x）</b></span>\r\n</p>', '2017-04-07 09:55:34.913389', '1', null, '6', '2017-04-07 09:55:34.913389', '2', null, '2');
INSERT INTO `article` VALUES ('8', '4', 'netcore', '测试', '<img src=\"http://localhost:14667/B-JUI/plugins/kindeditor_4.1.11/plugins/emoticons/images/32.gif\" border=\"0\" alt=\"\" />\r\n<div id=\"article_content\" class=\"article_content\">\r\n	<p>\r\n		<br />\r\n	</p>\r\n<pre class=\"prettyprint lang-php\">13124</pre>\r\nEF的Code First方式允许你先写Model,再通过Model生成<a href=\"http://lib.csdn.net/base/mysql\" class=\"replace_word\" target=\"_blank\">数据库</a>和表。\r\n	<p>\r\n		<br />\r\n	</p>\r\n	<p>\r\n		具体步骤如下：\r\n	</p>\r\n	<p>\r\n		1、建项目\r\n	</p>\r\n	<p>\r\n		2、在model文件夹中，添加一个派生自DbContext的类，和一些Model类。\r\n	</p>\r\n	<p>\r\n		3、修改web.Config中的连接字符串。\r\n	</p>\r\n	<p>\r\n		4、生成基架Controller。\r\n	</p>\r\n	<p>\r\n		这样就可以生成数据库和表。\r\n	</p>\r\n	<p>\r\n		但实际开发中难免会修改模型和派生的Context，修改后再次运行会出现异常，提示“<span style=\"color:#ff0000;\">支持“MyContext”上下文的模型已在数据库创建后发生更改。请考虑使用 Code First 迁移更新数据库”</span><span style=\"color:#000000;\">异常。</span> \r\n	</p>\r\n	<p>\r\n		可以通过如下步骤更新数据库，消除上述异常：\r\n	</p>\r\n	<p>\r\n		1、在项目的“程序包管理控制台\"窗口，输入”enable-migrations\"，回车，导致在项目中生成一个“Migrations\"文件夹，其中包含两个.cs类。\r\n	</p>\r\n	<p>\r\n		2、在1中生成的Configuration.cs中修改构造函数中代码为：”AutomaticMigrationsEnabled = true;“\r\n	</p>\r\n	<p>\r\n		3、保存修改后，在“程序包管理控制台\"中输入”update-database\"，回车，再次运行即可。\r\n	</p>\r\n</div>', '2017-03-21 15:37:11.151920', '0', null, '2', '0001-01-01 00:00:00.000000', '2', null, '6');
INSERT INTO `article` VALUES ('12', '2', '', '测试', '<p><img src=\"/upload/image/20170322/6362578661924131868443190.jpg\" title=\"1.jpg\" alt=\"1.jpg\"/>2342352<br/></p>', '2017-03-22 13:37:04.389522', '1', null, '2', '2017-03-29 09:27:13.653486', '2', null, '2');
INSERT INTO `article` VALUES ('14', '3', null, '测试', null, '2017-03-28 12:33:39.418065', '1', null, '2', '2017-03-28 12:33:39.462095', '2', '驳回测试', '3');
INSERT INTO `article` VALUES ('15', '4', null, '测试', '<img src=\"/KEupload/a0f11d63-20d1-4f8c-8c15-7c354f2f5a50.jpg\" alt=\"\" />', '2017-03-23 15:35:57.604857', '1', '23123', '2', '2017-03-28 12:22:44.283158', '2', null, '2');
INSERT INTO `article` VALUES ('16', '4', null, '测试', '1312<img src=\"/KEupload/bfb6296f-532a-4925-b44c-093dffcf9fcd.jpg\" alt=\"\" />', '2017-03-30 13:12:14.712469', '1', null, '2', '2017-03-30 13:12:24.520229', '2', null, '2');
INSERT INTO `article` VALUES ('17', '1', '测试', '测试', '214', '2017-03-30 10:52:06.859266', '1', null, '2', '2017-03-30 10:52:15.216448', '2', '驳回', '2');
INSERT INTO `article` VALUES ('18', '3', null, '测试', '<img src=\"/KEupload/3c76ccfd-e7e7-468e-b5a7-9cb476fcf3f4.jpg\" alt=\"\" />', '2017-03-30 13:14:00.399360', '1', null, '2', '2017-03-30 13:14:17.070874', '2', null, '2');
INSERT INTO `article` VALUES ('19', '1', null, '测试', '213123', '2017-03-30 15:31:11.910156', '1', null, '2', '2017-04-05 13:25:52.851064', '6', null, '2');
INSERT INTO `article` VALUES ('20', '3', '.NET Core', '简析.NET Core 以及与 .NET Framework的关系', '<p>\r\n	<span style=\"color:#333333;font-family:Arial;font-size:14px;background-color:#FFFFFF;\">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;至2002微软公司推出.NET平台已近15年，在互联网快速迭代的浪潮中，许多语言已被淘汰，同时也有更多新的语言涌现，但 .Net 依然坚挺的站在系统开发平台的一线阵营中，并且随着.NET Core 即将到来(2016年6月27日)的正式版，势必迎来新一轮春天。</span> \r\n</p>\r\n<p>\r\n	<span style=\"color:#333333;font-family:Arial;font-size:14px;background-color:#FFFFFF;\"> </span>\r\n</p>\r\n<h2 id=\"一-.net-的-framework-们\" style=\"color:#333333;font-family:Arial;background-color:#FFFFFF;\">\r\n	一 .NET 的 Framework 们\r\n</h2>\r\n<p style=\"color:#333333;font-family:Arial;font-size:14px;background-color:#FFFFFF;\">\r\n	&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;要理解.NET Core就有必要了解之前的.NET平台的众多Framework们。2002年微软公司发布的第一个.NET框架————.NET Framework，不久后又发布了.NET Compact Framework 用于在较小的移动设备（Windows mobile），而.NET Compact Framework 也含有一套 类似.NET Framework 体系（Runtime, Framework,Application Model），它是一个复制精简版的 .NET Framework。在数年间微软乐此不疲的推出了数个类似 .NET Framework的框架，以用于在不同的设备和平台上运行。每个Framework都有类似的体系但又不完全相同的，这样Framework越来越多，对开发者来说不一样的设备即便功能相同也需要维护多套代码，增加了开发的复杂度。\r\n</p>\r\n<p style=\"color:#333333;font-family:Arial;font-size:14px;background-color:#FFFFFF;\">\r\n	<br />\r\n</p>\r\n<h2 id=\"二-.net-core的到来\" style=\"color:#333333;font-family:Arial;background-color:#FFFFFF;\">\r\n	二 .NET Core的到来\r\n</h2>\r\n<p style=\"color:#333333;font-family:Arial;font-size:14px;background-color:#FFFFFF;\">\r\n	&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;微软对这些问题的重新思考和改进让.NET Core横空出世。<br />\r\n.NET Core是一个开源的模块化的Framework，不管是开发web或移动设备都在同一个Framework（.NET Core）下运行，而且 .NET Core也可在不同的<a href=\"http://lib.csdn.net/base/operatingsystem\" class=\"replace_word\" target=\"_blank\">操作系统</a>上运行，包括Windows、<a href=\"http://lib.csdn.net/base/linux\" class=\"replace_word\" target=\"_blank\">Linux</a>、MacOS，实现了跨平台跨设备。<br />\r\n&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;更棒的是.NET Core 在发布程序时不用事先安装Framework而是通过Nuget下载，这样在初次部署时就不用安装一个复杂而庞大Framework，而是按需下载。这种基于Nuget的按需加载铸就了.NET Core 跨平台。\r\n</p>\r\n<p style=\"color:#333333;font-family:Arial;font-size:14px;background-color:#FFFFFF;\">\r\n	.NET Core 构成体系如下：\r\n</p>\r\n<h3 id=\"runtime\" style=\"color:#333333;font-family:Arial;background-color:#FFFFFF;\">\r\n	1. Runtime\r\n</h3>\r\n<p style=\"color:#333333;font-family:Arial;font-size:14px;background-color:#FFFFFF;\">\r\n	&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;在.NET Core 中有实现两种RunTime，NativeRuntime 和 CoreCLR。NativeRuntime 将C# 或 VB<a href=\"http://lib.csdn.net/base/dotnet\" class=\"replace_word\" target=\"_blank\">.NET</a>&nbsp;代码直接转换为原生机器码。而CoreCLR是一个开源的JIT运行时，会将代码编译成中间语言（IL）在最终运行时再转换机器码。\r\n</p>\r\n<h3 id=\"unified-bcl\" style=\"color:#333333;font-family:Arial;background-color:#FFFFFF;\">\r\n	2. Unified BCL\r\n</h3>\r\n<p style=\"color:#333333;font-family:Arial;font-size:14px;background-color:#FFFFFF;\">\r\n	&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Base Classlibrary即基础类，例如 FileSystem、Console、XML操作等。\r\n</p>\r\n<h3 id=\"windows-store-appmodel-asp.net-core-1.0\" style=\"color:#333333;font-family:Arial;background-color:#FFFFFF;\">\r\n	3. Windows Store AppModel &amp; ASP.NET Core 1.0\r\n</h3>\r\n<p style=\"color:#333333;font-family:Arial;font-size:14px;background-color:#FFFFFF;\">\r\n	&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;提供开发Windows系统的各种触屏设备和ASP<a href=\"http://lib.csdn.net/base/dotnet\" class=\"replace_word\" target=\"_blank\">.Net</a>程序的一组基础库。\r\n</p>\r\n<h2 id=\"三-.net-core-与-.net-其他framework的关系\" style=\"color:#333333;font-family:Arial;background-color:#FFFFFF;\">\r\n	三 .NET Core 与 .NET 其他Framework的关系\r\n</h2>\r\n<h3 id=\"net-core-.net-framework\" style=\"color:#333333;font-family:Arial;background-color:#FFFFFF;\">\r\n	1 .NET Core &amp; .NET Framework\r\n</h3>\r\n<p style=\"color:#333333;font-family:Arial;font-size:14px;background-color:#FFFFFF;\">\r\n	&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;.NET Core &amp; .NET Framework 都是 NET Standard Library 的实现，一种是跨平台，而另一种是Windows下特有的，除实现NET Standard Library外 .NET Core 和 .NET Framework 都有各自特有的实现。.NET Standard Library 是跨操作系统的关键所在，它定义了一组接口，而每个操作系统有各自的实现，.NET Core通过nuget可以动态下载当前操作系统的实现，从而实现跨操作系统（暨跨操作系统共享代码）。\r\n</p>\r\n<p style=\"color:#333333;font-family:Arial;font-size:14px;background-color:#FFFFFF;\">\r\n	&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;.NET Core 更依赖于Nuget,允许本地application在部署时动态下载需要的class library。而.NET Framework需要在系统中预装。这样.NET Core的部署比.NET Framework 更简单快速同样也更轻便。\r\n</p>\r\n<h3 id=\"net-core-asp.net\" style=\"color:#333333;font-family:Arial;background-color:#FFFFFF;\">\r\n	2 .NET Core &amp; ASP.NET\r\n</h3>\r\n<p>\r\n	<br />\r\n</p>\r\n<p style=\"color:#333333;font-family:Arial;font-size:14px;background-color:#FFFFFF;\">\r\n	<strong>ASP.NET Core Web Application(.NET Core)</strong>\r\n</p>\r\n<p style=\"color:#333333;font-family:Arial;font-size:14px;background-color:#FFFFFF;\">\r\n	&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;依赖于.NET Core的ASP.NET 只可以调用.NET Core的API，可以在多个操作系统上运行。(可见下图下半部分的Reference Manager，在assemblies中没有.NET Framework 中的库)\r\n</p>\r\n<p style=\"color:#333333;font-family:Arial;font-size:14px;background-color:#FFFFFF;\">\r\n	<strong>ASP.NET Core Web Application(.NET Framework)</strong>\r\n</p>\r\n<p style=\"color:#333333;font-family:Arial;font-size:14px;background-color:#FFFFFF;\">\r\n	&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;依赖于.NET Core &amp; .NET Framework 的ASP.NET 可以调用.NET Core&amp;.NET Framework的API ，只可以在Windows下运行。(可见下图上半部分的Reference Manager，在assemblies中含有所有.NET Framework 中的库)\r\n</p>\r\n<h3 id=\"net-core-mono\" style=\"color:#333333;font-family:Arial;background-color:#FFFFFF;\">\r\n	3 .NET Core &amp; Mono\r\n</h3>\r\n<p style=\"color:#333333;font-family:Arial;font-size:14px;background-color:#FFFFFF;\">\r\n	&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Mono是.NET Framework的开源版本的实现，在某些特性上和.NET Core 很类似比如开源，跨操作系统。目前.NET CORE 会从Mono上吸取经验，发展壮大最终可以更好的在除Windows外的其他操作系统上运行。另外Mone支持的App Models要比.NET Core多（例如Windows Forms）。\r\n</p>\r\n<h2 id=\"总结\" style=\"color:#333333;font-family:Arial;background-color:#FFFFFF;\">\r\n	总结\r\n</h2>\r\n<p style=\"color:#333333;font-family:Arial;font-size:14px;background-color:#FFFFFF;\">\r\n	&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;微软CEO萨提亚·纳德拉睿智的决定抛弃以PC为中心，拥抱各种设备和平台，成为一家软件服务型企业。为了达成这种愿景必须构建新的微软生态链，囊括Windows,Linux,OS X及其他操作系统，覆盖X86/ARM等处理器以及不同的设备（包括PC,Phone,全息眼镜及其他）。这些足见微软的“野心”。随着.NET Core 正式发布大战略迈出坚实的一步，6月27日让我们拭目以待吧。\r\n</p>\r\n<br />\r\n<p>\r\n	<br />\r\n</p>', '2017-04-01 13:02:52.772631', '1', null, '6', '2017-04-05 13:25:45.922472', '6', null, '2');
INSERT INTO `article` VALUES ('21', '3', 'net core', 'net core 发送邮件', '<p style=\"color:#333333;font-family:Verdana, Arial, Helvetica, sans-serif;font-size:14px;background-color:#FFFFFF;\">\r\n	&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;准备将之前项目的邮件发送功能迁移到 asp.net core，先从封装类库入手，发现在 asp.net core 1.1中并示提供SMTP相关类库，问了下度娘，发现了MailKit。\r\n</p>\r\n<pre class=\"prettyprint lang-cs\">public static void Send(string email, string subject, string message)\r\n{\r\n&nbsp;&nbsp;&nbsp;&nbsp;var emailMessage = new MimeMessage();\r\n&nbsp;&nbsp;&nbsp;&nbsp;emailMessage.From.Add(new MailboxAddress(\"金帆系统\", \"******@163.com\"));\r\n&nbsp;&nbsp;&nbsp;&nbsp;emailMessage.To.Add(new MailboxAddress(\"mail\", email));  emailMessage.Subject = subject;\r\n&nbsp;&nbsp;&nbsp;&nbsp;emailMessage.Body = new TextPart(\"plain\") { Text = message };\r\n\r\n&nbsp;&nbsp;&nbsp;&nbsp;using (var client = new SmtpClient())\r\n&nbsp;&nbsp;&nbsp;&nbsp;{\r\n&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;client.Connect(\"smtp.163.com\", 465, true);\r\n&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;client.Authenticate(\"*****@163.com\", \"密码\");\r\n\r\n&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;client.Send(emailMessage);\r\n&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;client.Disconnect(true);\r\n&nbsp;&nbsp;&nbsp;&nbsp;}\r\n}\r\n</pre>\r\n<p>\r\n	<br />\r\n</p>', '2017-04-01 13:23:47.659138', '1', null, '6', '2017-04-05 13:25:37.998423', '6', null, '2');
INSERT INTO `article` VALUES ('22', '3', 'supervisor', 'ASP.NET Core 发布至Ubuntu14.04(supervisor 进程管理)', '<h3 style=\"background:white;\">\r\n	1. 安装.NET Core SDK\r\n</h3>\r\n<pre class=\"prettyprint lang-cs\">sudo sh -c \'echo \"deb [arch=amd64] https://apt-mo.trafficmanager.net/repos/dotnet/ trusty main\" &gt; /etc/apt/sources.list.d/dotnetdev.list\'\r\nsudo apt-key adv --keyserver apt-mo.trafficmanager.net --recv-keys 417A0893\r\nsudo apt-get update\r\nsudo apt-get install dotnet-dev-1.0.0-preview2-003121</pre>\r\n<p class=\"MsoNormal\">\r\n	安装好以后，就可以使用 dotnet 命令\r\n</p>\r\n<h3 style=\"background:white;\">\r\n	2.安装及配置Nginx\r\n</h3>\r\n<pre class=\"prettyprint lang-js\">sudo apt-get install nginx</pre>\r\n<h3 style=\"background:white;\">\r\n	3.安装及配置supervisor\r\n</h3>\r\n<pre class=\"prettyprint lang-js\">sudo apt-get install supervisor</pre>\r\n<p style=\"background:white;\">\r\n	安装好&nbsp;supervisor 以后，下面就来配置。\r\n</p>\r\n<p style=\"background:white;\">\r\n	<span style=\"font-size:11.5pt;color:#414A51;\">定位到</span><span style=\"font-size:11.5pt;font-family:&quot;color:#414A51;\">&nbsp;/etc/supervisor/conf.d/ </span><span style=\"font-size:11.5pt;color:#414A51;\">文件夹，添加一个</span><span style=\"font-size:11.5pt;font-family:&quot;color:#414A51;\">JFnetcore.conf </span><span style=\"font-size:11.5pt;color:#414A51;\">文件。</span><span style=\"font-size:11.5pt;font-family:&quot;color:#414A51;\"></span> \r\n</p>\r\n下面是dotnet&nbsp;JFnetcore.dll 命令方式。经过多次的坑，终于得到最终配置文件。\r\n<pre class=\"prettyprint lang-js\">[program:JFnetcore]\r\ncommand=/usr/bin/dotnet /home/jfngrok/JFnetcore/JFnetcore.dll\r\ndirectory=/home/jfngrok/JFnetcore\r\nautostart=true\r\nautorestart=true\r\nstderr_logfile=/var/log/NetCoreBBS.err.log\r\nstdout_logfile=/var/log/NetCoreBBS.out.log\r\nenvironment=ASPNETCORE__ENVIRONMENT=Production\r\nuser=root\r\nstopsignal=INT</pre>\r\n<p style=\"background:white;\">\r\n	重启&nbsp;supervisor\r\n</p>\r\n<pre class=\"prettyprint lang-js\">sudo service supervisor restart</pre>\r\n<p>\r\n<pre class=\"prettyprint lang-js\">更新新的配置到supervisord\r\nsupervisorctl update\r\n重新启动配置中的所有程序\r\nsupervisorctl reload\r\n启动某个进程(program_name=你配置中写的程序名称)\r\nsupervisorctl start program_name\r\n查看正在守候的进程\r\nsupervisorctl\r\n停止某一进程 (program_name=你配置中写的程序名称)\r\npervisorctl stop program_name\r\n重启某一进程 (program_name=你配置中写的程序名称)\r\nsupervisorctl restart program_name\r\n停止全部进程\r\nsupervisorctl stop all\r\n注意：显示用stop停止掉的进程，用reload或者update都不会自动重启。</pre>\r\n</p>', '2017-04-05 19:24:51.421548', '1', null, '6', '2017-04-05 19:24:51.421548', '2', null, '2');
INSERT INTO `article` VALUES ('23', '3', 'net core', 'net core 发布至IIS', '<p>\r\n	<span style=\"font-size:16px;\">1.net core发布至IIS需要安装从IIS到Kestrel server的反向代理，官方下载地址：</span><a href=\"https://go.microsoft.com/fwlink/?LinkID=827547\" target=\"_blank\"><span style=\"font-size:16px;\">DotNetCore.1.0.1-WindowsHosting.exe</span></a> \r\n</p>\r\n<p>\r\n	<br />\r\n</p>\r\n<p>\r\n	<span style=\"font-size:16px;\">安装完后重启服务，用管理员权限打开命令行：</span> \r\n</p>\r\n<p>\r\n	<span style=\"font-size:16px;\">输入&nbsp;</span> \r\n</p>\r\n<p>\r\n	<br />\r\n</p>\r\n<pre class=\"prettyprint lang-js\">net stop was /y</pre>\r\n<p>\r\n	<br />\r\n</p>\r\n<p>\r\n	<span style=\"font-size:16px;\">运行完后再运行</span> \r\n</p>\r\n<p>\r\n	<br />\r\n</p>\r\n<pre class=\"prettyprint lang-js\">net start w3svc</pre>\r\n<p>\r\n	<br />\r\n</p>\r\n<p>\r\n	<img src=\"/KEupload/e44ef6d8-6fe5-4caf-b49a-22cba11c60ca.png\" width=\"800\" height=\"468\" alt=\"\" /> \r\n</p>\r\n<p>\r\n	<br />\r\n</p>\r\n<p>\r\n	<span style=\"font-size:16px;\">2.在VS2017中发布网站</span> \r\n</p>\r\n<p>\r\n	<img src=\"/KEupload/047ebe9a-6704-4d07-9e3c-73d463cb2f54.png\" width=\"600\" height=\"391\" alt=\"\" /> \r\n</p>\r\n<p>\r\n	<img src=\"/KEupload/ab0d5e02-6d93-4de6-949c-23f8f5f9b9ca.png\" width=\"600\" height=\"471\" alt=\"\" /> \r\n</p>\r\n<p>\r\n	<img src=\"/KEupload/2a483763-0864-4ca2-942a-86f185f69a84.png\" width=\"600\" height=\"471\" alt=\"\" />\r\n</p>\r\n<p>\r\n	<span style=\"font-size:16px;\">3.设置IIS</span> \r\n</p>\r\n<p>\r\n	<img src=\"/KEupload/9ff77ddc-cef5-4b63-86bc-3460af4b4b58.png\" alt=\"\" /> \r\n</p>', '2017-04-07 09:24:23.632737', '1', null, '6', '2017-04-07 09:24:23.632737', '2', null, '2');
INSERT INTO `article` VALUES ('24', '3', null, '.net core 浅谈', '<p>\r\n	一\r\n.net的Framework\r\n</p>\r\n<p>\r\n	<br />\r\n</p>\r\n<p style=\"margin-left:0in;\">\r\n	2002年微软发布第一个.NET框架——.NET Framework 1.0。\r\n</p>\r\n<p style=\"margin-left:0in;\">\r\n	1.0版本之后发布了.NET Compact Framework 用于在较小的移动设备开发。它是一个精简版的 .NET Framework加移动设备独有项目的支持模块。\r\n</p>\r\n<p style=\"margin-left:0in;\">\r\n	<br />\r\n</p>\r\n<p style=\"margin-left:0in;\">\r\n	二\r\n.net\r\n的跨平台——mono\r\n</p>\r\n<p style=\"margin-left:0in;\">\r\n	<br />\r\n</p>\r\n<p style=\"margin-left:0in;\">\r\n	Mono于2004年7月30日发布1.0版本。\r\n</p>\r\n<p style=\"margin-left:0in;\">\r\n	跨平台： Windows、Unix\r\n(Linux、 Solaris&nbsp; 、FreeBSD)、Mac OSX、iOS、Android\r\n</p>\r\n<p style=\"margin-left:0in;\">\r\n	基于.NET\r\nFramework的技术，例如ASP NET，ADO NET和WinForms，这些技术在Mono中至今还没有被完全地实现。\r\n</p>\r\n<p style=\"margin-left:0in;\">\r\n	<br />\r\n</p>\r\n<p style=\"margin-left:0in;\">\r\n	三\r\n.net\r\ncore的到来\r\n</p>\r\n<p style=\"margin-left:0in;\">\r\n	<br />\r\n</p>\r\n<p style=\"margin-left:0in;\">\r\n	去年微软发布.net core 1.0，一个开源的模块化的Framework。\r\n</p>\r\n<p style=\"margin-left:0in;\">\r\n	跨平台： win、linux、MacOS\r\n</p>\r\n<p style=\"margin-left:0in;\">\r\n	发布程序不用安装Framework而是通过Nuget下载.net core SDK.\r\n</p>\r\n<p style=\"margin-left:0in;\">\r\n	<br />\r\n</p>\r\n四\r\n.net\r\ncore与.net的区别\r\n<p>\r\n	<br />\r\n</p>\r\n<p style=\"margin-left:0in;\">\r\n	<table border=\"0\" cellpadding=\"0\" cellspacing=\"0\" width=\"1002\" style=\"width:752pt;\" class=\"ke-zeroborder\">\r\n		<tbody>\r\n			<tr>\r\n				<td height=\"49\" class=\"oa1\" width=\"502\">\r\n					<p style=\"margin-left:0in;text-align:center;\">\r\n						<span style=\"font-size:18.0pt;font-family:&quot;color:white;font-weight:bold;\">.NET</span><span style=\"font-size:18.0pt;font-family:&quot;color:white;font-weight:bold;\"> </span>\r\n					</p>\r\n				</td>\r\n				<td colspan=\"2\" class=\"oa1\" width=\"500\">\r\n					<p style=\"margin-left:0in;text-align:center;\">\r\n						<span style=\"font-size:18.0pt;font-family:&quot;color:white;font-weight:bold;\">.NET CORE</span><span style=\"font-size:18.0pt;font-family:&quot;color:white;font-weight:bold;\"> </span>\r\n					</p>\r\n				</td>\r\n			</tr>\r\n			<tr>\r\n				<td height=\"49\" class=\"oa3\" width=\"502\">\r\n					<p style=\"margin-left:0in;text-align:center;\">\r\n						<span style=\"font-size:18pt;font-family:&quot;\">vb.net</span><span style=\"font-size:18pt;font-family:华文仿宋;\">、</span><span style=\"font-size:18pt;font-family:&quot;\">C#...</span><span style=\"font-size:18pt;font-family:&quot;\"> </span>\r\n					</p>\r\n				</td>\r\n				<td class=\"oa3\" width=\"299\">\r\n					<p style=\"margin-left:0in;text-align:center;\">\r\n						<span style=\"font-size:18pt;font-family:&quot;\">Asp.net</span><span style=\"font-size:18pt;font-family:&quot;\"> core</span><span style=\"font-size:18pt;font-family:&quot;\"> </span>\r\n					</p>\r\n				</td>\r\n				<td class=\"oa3\" width=\"200\">\r\n					<p style=\"margin-left:0in;text-align:center;\">\r\n						<span style=\"font-size:18pt;font-family:&quot;\">UWP</span><span style=\"font-size:18pt;font-family:&quot;\"> </span>\r\n					</p>\r\n				</td>\r\n			</tr>\r\n			<tr>\r\n				<td colspan=\"3\" height=\"49\" class=\"oa4\" width=\"1002\">\r\n					<p style=\"margin-left:0in;text-align:center;\">\r\n						<span style=\"font-size:18pt;font-family:华文仿宋;\">不同的编译器</span><span style=\"font-size:18pt;font-family:&quot;\"> </span>\r\n					</p>\r\n				</td>\r\n			</tr>\r\n			<tr>\r\n				<td colspan=\"3\" height=\"49\" class=\"oa5\" width=\"1002\">\r\n					<p style=\"margin-left:0in;text-align:center;\">\r\n						<span style=\"font-size:18pt;font-family:&quot;\">IL</span><span style=\"font-size:18pt;font-family:&quot;\"> </span>\r\n					</p>\r\n				</td>\r\n			</tr>\r\n			<tr>\r\n				<td height=\"49\" class=\"oa4\" width=\"502\">\r\n					<p style=\"margin-left:0in;text-align:center;\">\r\n						<span style=\"font-size:18pt;font-family:华文仿宋;\">编译器 </span><span style=\"font-size:18pt;font-family:&quot;\">JIT</span><span style=\"font-size:18pt;font-family:&quot;\"> </span>\r\n					</p>\r\n				</td>\r\n				<td colspan=\"2\" class=\"oa4\" width=\"500\">\r\n					<p style=\"margin-left:0in;text-align:center;\">\r\n						<span style=\"font-size:18pt;font-family:华文仿宋;\">编译器</span><span style=\"font-size:18pt;font-family:&quot;vertical-align:baseline;\"> </span><span style=\"font-size:18pt;font-family:&quot;\">LLILC</span><span style=\"font-size:18pt;font-family:华文仿宋;\">（</span><span style=\"font-size:18pt;font-family:&quot;\">JIT+AOT</span><span style=\"font-size:18pt;font-family:华文仿宋;\">）</span><span style=\"font-size:18pt;font-family:&quot;\"> </span>\r\n					</p>\r\n				</td>\r\n			</tr>\r\n			<tr>\r\n				<td colspan=\"3\" height=\"49\" class=\"oa5\" width=\"1002\">\r\n					<p style=\"margin-left:0in;text-align:center;\">\r\n						<span style=\"font-size:18pt;font-family:华文仿宋;\">机器码</span><span style=\"font-size:18pt;font-family:&quot;\"> </span>\r\n					</p>\r\n				</td>\r\n			</tr>\r\n		</tbody>\r\n	</table>\r\n<br />\r\n五\r\n.net\r\ncore 的变化<br />\r\n1.反射API\r\n</p>\r\n<p style=\"margin-left:0in;\">\r\n	2.应用程序域 Application\r\nDomain\r\n</p>\r\n<p style=\"margin-left:0in;\">\r\n	3.Remoting\r\n</p>\r\n<p style=\"margin-left:0in;\">\r\n	4.二进制序列化\r\n</p>\r\n<p style=\"margin-left:0in;\">\r\n	5.其他\r\n</p>\r\n<p style=\"margin-left:0.38in;text-indent:-0.38in;\">\r\n	&nbsp; System.Data\r\n</p>\r\n<p style=\"margin-left:0.38in;text-indent:-0.38in;\">\r\n	&nbsp; System.Drawing\r\n</p>\r\n<p style=\"margin-left:0.38in;text-indent:-0.38in;\">\r\n	&nbsp; System.Transactions\r\n</p>\r\n<p style=\"margin-left:0.38in;text-indent:-0.38in;\">\r\n	&nbsp; System.Net.Mail\r\n</p>\r\n<p style=\"margin-left:0.38in;text-indent:-0.38in;\">\r\n	&nbsp; System.Xml.Xsl 与 System.Xml.Schema\r\n</p>\r\n<p style=\"margin-left:0.38in;text-indent:-0.38in;\">\r\n	&nbsp; System.IO.Ports\r\n</p>\r\n<p style=\"margin-left:0.38in;text-indent:-0.38in;\">\r\n	&nbsp; System.Workflow\r\n</p>\r\n<p style=\"margin-left:0.38in;text-indent:-0.38in;\">\r\n	&nbsp; System.Xaml\r\n</p>\r\n<p style=\"margin-left:0.38in;text-indent:-0.38in;\">\r\n	<br />\r\n</p>\r\n<p style=\"margin-left:0.38in;text-indent:-0.38in;\">\r\n	六\r\n实际开发总结\r\n</p>\r\n<p style=\"margin-left:0.38in;text-indent:-0.38in;\">\r\n	<span>1.MySql数据库的VS中间插件只支持到VS2015，所以只能用code first模式。</span>\r\n	<p style=\"margin-left:0in;\">\r\n		<span>\r\n		<p style=\"margin-left:0in;\">\r\n			2.MySQL.Data.EntityFrameworkCore&nbsp; 支持.net core 1.0，<span>改用Pomelo.EntityFrameworkCore.MySql</span>\r\n		</p>\r\n		<p style=\"margin-left:0in;\">\r\n			<span>\r\n			<p style=\"margin-left:0in;\">\r\n				3.必须引入Microsoft.EntityFrameworkCore，没有任何错误相关提示。\r\n			</p>\r\n			<p style=\"margin-left:0in;\">\r\n				<span>\r\n				<p style=\"margin-left:0in;\">\r\n					4.EF core 不支持code\r\nfirst生成数据库视图，但是手动创建的mysql视图可以访问。\r\n				</p>\r\n				<p style=\"margin-left:0in;\">\r\n					<span>\r\n					<p style=\"margin-left:0in;\">\r\n						5.Session不稳定，设置的600min，容易丢失，可能是服务器问题。\r\n					</p>\r\n					<p style=\"margin-left:0in;\">\r\n						<span></span><span></span><span>6.静态文件的访问需要添加权限</span><span>\r\n						<p style=\"margin-left:0in;\">\r\n							7. 发布Linux时的问题，系统默认大小写敏感，“\\”不识别, 安装supervisor 进程管理\r\n						</p>\r\n						<p style=\"margin-left:0in;\">\r\n							8.发布IIS时的问题，需要安装Kestrel server的反向代理\r\n						</p>\r\n</span>\r\n					</p>\r\n</span>\r\n				</p>\r\n</span>\r\n			</p>\r\n</span>\r\n		</p>\r\n</span>\r\n	</p>\r\n</p>\r\n<p style=\"margin-left:0.38in;text-indent:-0.38in;\">\r\n	<br />\r\n</p>\r\n<p style=\"margin-left:0.38in;text-indent:-0.38in;\">\r\n</p>\r\n<p>\r\n	<br />\r\n</p>\r\n<p>\r\n	<br />\r\n</p>', '2017-04-25 14:24:32.589518', '1', null, '6', '2017-04-25 14:24:32.589518', '2', null, '2');
INSERT INTO `article` VALUES ('25', '4', null, '123', '142', '2017-04-28 14:47:53.132922', '1', null, '6', '2017-04-28 14:47:53.132922', '2', null, '2');

-- ----------------------------
-- Table structure for `articletype`
-- ----------------------------
DROP TABLE IF EXISTS `articletype`;
CREATE TABLE `articletype` (
  `Id` int(11) NOT NULL AUTO_INCREMENT,
  `ArticleType_name` longtext,
  `Createtime` datetime(6) NOT NULL,
  `Disable` int(11) NOT NULL,
  `Disabledesc` longtext,
  `User_Id` int(11) NOT NULL,
  PRIMARY KEY (`Id`),
  KEY `articletype_userid` (`User_Id`),
  CONSTRAINT `articletype_userid` FOREIGN KEY (`User_Id`) REFERENCES `users` (`Id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of articletype
-- ----------------------------
INSERT INTO `articletype` VALUES ('1', 'C#', '2017-03-30 09:57:30.000000', '1', null, '2');
INSERT INTO `articletype` VALUES ('2', 'C', '2017-03-30 09:57:43.000000', '1', null, '2');
INSERT INTO `articletype` VALUES ('3', 'Net core', '2017-03-30 09:58:00.000000', '1', null, '2');
INSERT INTO `articletype` VALUES ('4', '其他', '2017-04-28 13:47:38.138155', '1', null, '2');

-- ----------------------------
-- Table structure for `config`
-- ----------------------------
DROP TABLE IF EXISTS `config`;
CREATE TABLE `config` (
  `Id` int(11) NOT NULL AUTO_INCREMENT,
  `Config_name` longtext,
  `Config_remark` longtext,
  `Config_value` longtext,
  PRIMARY KEY (`Id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of config
-- ----------------------------
INSERT INTO `config` VALUES ('1', 'audit', '论坛文章审核开关', '0');
INSERT INTO `config` VALUES ('2', 'replay', '文章回复开关', '1');

-- ----------------------------
-- Table structure for `log`
-- ----------------------------
DROP TABLE IF EXISTS `log`;
CREATE TABLE `log` (
  `Id` int(11) NOT NULL AUTO_INCREMENT,
  `Createtime` datetime(6) NOT NULL,
  `Log_ip` longtext,
  `Log_operation` int(11) NOT NULL,
  `Log_remark` longtext,
  `Log_userid` int(11) NOT NULL,
  PRIMARY KEY (`Id`),
  KEY `log_userid` (`Log_userid`),
  KEY `log_operationid` (`Log_operation`),
  CONSTRAINT `log_operationid` FOREIGN KEY (`Log_operation`) REFERENCES `operation` (`Id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `log_userid` FOREIGN KEY (`Log_userid`) REFERENCES `users` (`Id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=612 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of log
-- ----------------------------
INSERT INTO `log` VALUES ('231', '2017-04-01 08:13:54.603145', '::1', '1', '', '6');
INSERT INTO `log` VALUES ('232', '2017-04-01 08:20:45.355028', '::1', '1', '', '6');
INSERT INTO `log` VALUES ('233', '2017-04-01 08:21:06.515781', '::1', '7', '上传文件chrome上传测试.rar,', '6');
INSERT INTO `log` VALUES ('234', '2017-04-01 08:21:44.168941', '::1', '1', '', '6');
INSERT INTO `log` VALUES ('235', '2017-04-01 08:21:57.829089', '::1', '7', '上传文件IE上传测试 .rar,', '6');
INSERT INTO `log` VALUES ('236', '2017-04-01 08:25:34.002364', '::1', '1', '', '6');
INSERT INTO `log` VALUES ('237', '2017-04-01 08:25:56.635923', '::1', '7', '上传文件IE上传测试 .rar,', '6');
INSERT INTO `log` VALUES ('238', '2017-04-01 10:34:13.442194', '::1', '1', '', '6');
INSERT INTO `log` VALUES ('239', '2017-04-01 12:32:55.908523', '::1', '1', '', '6');
INSERT INTO `log` VALUES ('240', '2017-04-01 12:39:54.744188', '::1', '1', '', '6');
INSERT INTO `log` VALUES ('241', '2017-04-01 12:44:05.362027', '::1', '1', '', '6');
INSERT INTO `log` VALUES ('242', '2017-04-01 12:47:13.218940', '127.0.0.1', '1', '', '6');
INSERT INTO `log` VALUES ('243', '2017-04-01 12:49:18.402068', '::1', '1', '', '6');
INSERT INTO `log` VALUES ('244', '2017-04-01 13:11:44.447936', '::1', '1', '', '6');
INSERT INTO `log` VALUES ('245', '2017-04-01 13:12:23.638240', '::1', '4', '修改参数[audit]为:0', '6');
INSERT INTO `log` VALUES ('246', '2017-04-02 08:18:18.676340', '::1', '1', '', '6');
INSERT INTO `log` VALUES ('247', '2017-04-02 08:29:47.971368', '::1', '1', '', '6');
INSERT INTO `log` VALUES ('248', '2017-04-04 18:57:19.471988', '222.92.69.162', '1', '', '6');
INSERT INTO `log` VALUES ('249', '2017-04-04 19:02:15.149785', '222.92.69.162', '7', '上传文件chrome上传测试.rar,', '6');
INSERT INTO `log` VALUES ('250', '2017-04-04 19:03:06.253748', '222.92.69.162', '7', '上传文件chrome上传测试4.5.rar,', '6');
INSERT INTO `log` VALUES ('251', '2017-04-04 19:09:53.318228', '222.92.69.162', '1', '', '6');
INSERT INTO `log` VALUES ('252', '2017-04-05 10:20:12.435264', '::1', '1', '', '6');
INSERT INTO `log` VALUES ('253', '2017-04-05 12:13:01.292347', '::1', '1', '', '6');
INSERT INTO `log` VALUES ('254', '2017-04-05 12:17:54.810749', '::1', '1', '', '6');
INSERT INTO `log` VALUES ('255', '2017-04-05 12:20:37.413481', '::1', '1', '', '6');
INSERT INTO `log` VALUES ('256', '2017-04-05 12:22:19.699200', '::1', '2', '', '6');
INSERT INTO `log` VALUES ('257', '2017-04-05 13:20:57.367167', '::1', '1', '', '6');
INSERT INTO `log` VALUES ('258', '2017-04-05 13:21:20.876611', '::1', '6', '审核通过文章:审核 参数测试，开启时', '6');
INSERT INTO `log` VALUES ('259', '2017-04-05 13:25:18.656688', '::1', '1', '', '6');
INSERT INTO `log` VALUES ('260', '2017-04-05 13:25:38.641850', '::1', '9', '审核通过文章:net core 发送邮件', '6');
INSERT INTO `log` VALUES ('261', '2017-04-05 13:25:46.391783', '::1', '9', '审核通过文章:简析.NET Core 以及与 .NET Framework的关系', '6');
INSERT INTO `log` VALUES ('262', '2017-04-05 13:25:53.308368', '::1', '9', '审核通过文章:审核 参数测试，开启时', '6');
INSERT INTO `log` VALUES ('263', '2017-04-04 22:34:59.814425', '222.92.69.162', '1', '', '6');
INSERT INTO `log` VALUES ('264', '2017-04-05 13:39:13.034232', '::1', '1', '', '6');
INSERT INTO `log` VALUES ('265', '2017-04-04 22:41:35.014241', '222.92.69.162', '1', '', '6');
INSERT INTO `log` VALUES ('266', '2017-04-04 22:42:20.937949', '222.92.69.162', '1', '', '2');
INSERT INTO `log` VALUES ('267', '2017-04-04 23:23:25.764983', '222.92.69.162', '1', '', '6');
INSERT INTO `log` VALUES ('268', '2017-04-05 00:57:57.766969', '222.92.69.162', '1', '', '6');
INSERT INTO `log` VALUES ('269', '2017-04-05 18:05:35.019540', '222.92.69.162', '1', '', '6');
INSERT INTO `log` VALUES ('270', '2017-04-06 09:20:35.421600', '::1', '1', '', '6');
INSERT INTO `log` VALUES ('271', '2017-04-05 18:33:22.521340', '222.92.69.162', '1', '', '6');
INSERT INTO `log` VALUES ('272', '2017-04-05 18:49:53.052063', '222.92.69.162', '6', '编辑文章:ASP.NET Core 发布至Ubuntu14.04(supervisor 进程管理)', '6');
INSERT INTO `log` VALUES ('273', '2017-04-05 18:51:54.758785', '222.92.69.162', '6', '编辑文章:ASP.NET Core 发布至Ubuntu14.04(supervisor 进程管理)', '6');
INSERT INTO `log` VALUES ('274', '2017-04-05 19:23:32.191554', '222.92.69.162', '1', '', '6');
INSERT INTO `log` VALUES ('275', '2017-04-05 19:24:52.252582', '222.92.69.162', '6', '编辑文章:ASP.NET Core 发布至Ubuntu14.04(supervisor 进程管理)', '6');
INSERT INTO `log` VALUES ('276', '2017-04-05 21:18:07.083026', '222.92.69.162', '2', '', '6');
INSERT INTO `log` VALUES ('277', '2017-04-06 12:49:39.686700', '::1', '1', '', '6');
INSERT INTO `log` VALUES ('278', '2017-04-06 12:58:28.257764', '::1', '2', '', '6');
INSERT INTO `log` VALUES ('279', '2017-04-06 12:58:35.282969', '::1', '1', '', '6');
INSERT INTO `log` VALUES ('280', '2017-04-05 22:37:03.830135', '222.92.69.162', '1', '', '6');
INSERT INTO `log` VALUES ('281', '2017-04-05 23:19:58.526915', '222.92.69.162', '1', '游客按钮直接登录', '1');
INSERT INTO `log` VALUES ('282', '2017-04-05 23:22:55.657473', '222.92.69.162', '1', '', '6');
INSERT INTO `log` VALUES ('283', '2017-04-05 23:31:13.864014', '222.92.69.162', '2', '', '6');
INSERT INTO `log` VALUES ('284', '2017-04-05 23:34:54.107377', '222.92.69.162', '1', '', '6');
INSERT INTO `log` VALUES ('285', '2017-04-06 14:41:48.705675', '::1', '1', '游客按钮直接登录', '1');
INSERT INTO `log` VALUES ('286', '2017-04-06 14:57:06.266220', '::1', '1', '', '6');
INSERT INTO `log` VALUES ('287', '2017-04-06 14:57:18.360235', '::1', '2', '', '6');
INSERT INTO `log` VALUES ('288', '2017-04-06 14:59:31.380127', '::1', '1', '', '6');
INSERT INTO `log` VALUES ('289', '2017-04-06 15:00:06.938257', '::1', '1', '', '6');
INSERT INTO `log` VALUES ('290', '2017-04-06 15:00:58.492798', '::1', '2', '', '6');
INSERT INTO `log` VALUES ('291', '2017-04-06 15:04:36.960739', '::1', '1', '', '6');
INSERT INTO `log` VALUES ('292', '2017-04-06 15:09:24.742485', '::1', '1', '', '6');
INSERT INTO `log` VALUES ('293', '2017-04-06 15:10:44.311606', '::1', '2', '', '6');
INSERT INTO `log` VALUES ('294', '2017-04-06 00:14:47.070768', '222.92.69.162', '1', '', '6');
INSERT INTO `log` VALUES ('295', '2017-04-06 15:25:52.105702', '::1', '1', '', '6');
INSERT INTO `log` VALUES ('296', '2017-04-06 15:31:10.283651', '::1', '1', '', '6');
INSERT INTO `log` VALUES ('297', '2017-04-06 15:33:02.842641', '::1', '2', '', '6');
INSERT INTO `log` VALUES ('298', '2017-04-06 00:44:38.796237', '222.92.69.162', '1', '', '6');
INSERT INTO `log` VALUES ('299', '2017-04-06 01:53:39.459540', '222.92.69.162', '2', '', '6');
INSERT INTO `log` VALUES ('300', '2017-04-07 08:50:53.160455', '::1', '1', '', '6');
INSERT INTO `log` VALUES ('301', '2017-04-07 08:52:35.922990', '192.168.0.245', '1', '', '2');
INSERT INTO `log` VALUES ('302', '2017-04-07 09:10:34.743257', '::1', '1', '', '6');
INSERT INTO `log` VALUES ('303', '2017-04-07 09:18:07.203884', '::1', '4', '编辑文章:net core 发布至IIS', '6');
INSERT INTO `log` VALUES ('304', '2017-04-07 09:20:15.422703', '::1', '4', '编辑文章:net core 发布至IIS', '6');
INSERT INTO `log` VALUES ('305', '2017-04-07 09:21:16.146073', '::1', '4', '编辑文章:net core 发布至IIS', '6');
INSERT INTO `log` VALUES ('306', '2017-04-07 09:24:24.146982', '::1', '4', '编辑文章:net core 发布至IIS', '6');
INSERT INTO `log` VALUES ('307', '2017-04-07 09:28:05.507478', '::1', '4', '编辑文章:U盘启动安装说明', '6');
INSERT INTO `log` VALUES ('308', '2017-04-07 09:33:12.917207', '::1', '1', '', '6');
INSERT INTO `log` VALUES ('309', '2017-04-07 09:50:48.068464', '::1', '1', '', '6');
INSERT INTO `log` VALUES ('310', '2017-04-07 09:55:35.817988', '::1', '4', '编辑文章:U盘启动安装说明', '6');
INSERT INTO `log` VALUES ('311', '2017-04-07 10:12:40.501241', '::1', '1', '', '6');
INSERT INTO `log` VALUES ('312', '2017-04-07 10:14:56.602472', '::1', '1', '', '6');
INSERT INTO `log` VALUES ('328', '2017-04-07 13:49:13.295018', '::1', '2', '', '6');
INSERT INTO `log` VALUES ('355', '2017-04-10 16:39:32.714668', '::1', '1', '', '6');
INSERT INTO `log` VALUES ('356', '2017-04-11 08:07:48.335276', '::1', '1', '', '6');
INSERT INTO `log` VALUES ('357', '2017-04-12 08:13:09.077505', '::1', '1', '', '6');
INSERT INTO `log` VALUES ('358', '2017-04-12 12:48:36.571031', '::1', '1', '游客按钮直接登录', '1');
INSERT INTO `log` VALUES ('359', '2017-04-12 12:49:04.397994', '::1', '2', '', '1');
INSERT INTO `log` VALUES ('360', '2017-04-12 12:49:06.484577', '::1', '1', '', '6');
INSERT INTO `log` VALUES ('376', '2017-04-25 14:13:45.796764', '::1', '1', '游客按钮直接登录', '1');
INSERT INTO `log` VALUES ('377', '2017-04-25 14:13:56.011928', '::1', '2', '', '1');
INSERT INTO `log` VALUES ('378', '2017-04-25 14:13:58.286545', '::1', '1', '', '6');
INSERT INTO `log` VALUES ('379', '2017-04-25 14:24:33.153890', '::1', '4', '编辑文章:.net core 浅谈', '6');
INSERT INTO `log` VALUES ('380', '2017-04-25 14:34:07.764464', '::1', '4', '修改角色信息权限 roleid:3,menuid:1,grade:3,disable:1', '6');
INSERT INTO `log` VALUES ('381', '2017-04-25 14:34:18.432662', '::1', '4', '修改角色信息权限 roleid:3,menuid:26,grade:2,disable:0', '6');
INSERT INTO `log` VALUES ('382', '2017-04-27 10:56:50.000795', '::1', '1', '游客按钮直接登录', '1');
INSERT INTO `log` VALUES ('383', '2017-04-27 10:58:49.231168', '::1', '1', '游客按钮直接登录', '1');
INSERT INTO `log` VALUES ('384', '2017-04-27 10:59:33.716486', '::1', '1', '', '6');
INSERT INTO `log` VALUES ('385', '2017-04-27 12:28:45.427805', '::1', '1', '', '6');
INSERT INTO `log` VALUES ('386', '2017-04-27 12:54:40.802582', '::1', '1', '', '6');
INSERT INTO `log` VALUES ('387', '2017-04-27 13:02:33.214945', '::1', '1', '', '6');
INSERT INTO `log` VALUES ('388', '2017-04-27 13:02:59.512199', '::1', '1', '', '6');
INSERT INTO `log` VALUES ('389', '2017-04-27 13:06:54.770483', '::1', '1', '', '6');
INSERT INTO `log` VALUES ('390', '2017-04-27 13:12:52.326525', '::1', '1', '', '6');
INSERT INTO `log` VALUES ('391', '2017-04-27 13:21:37.966955', '::1', '1', '', '6');
INSERT INTO `log` VALUES ('392', '2017-04-27 13:27:01.292480', '::1', '1', '', '6');
INSERT INTO `log` VALUES ('393', '2017-04-27 13:32:09.800154', '::1', '1', '', '6');
INSERT INTO `log` VALUES ('394', '2017-04-27 13:44:39.443983', '::1', '1', '', '6');
INSERT INTO `log` VALUES ('395', '2017-04-27 13:45:50.449943', '::1', '1', '', '6');
INSERT INTO `log` VALUES ('396', '2017-04-27 14:21:59.206359', '::1', '1', '', '6');
INSERT INTO `log` VALUES ('397', '2017-04-27 15:23:38.465362', '::1', '1', '', '6');
INSERT INTO `log` VALUES ('398', '2017-04-27 15:28:29.954905', '::1', '1', '', '6');
INSERT INTO `log` VALUES ('399', '2017-04-27 15:34:17.004124', '::1', '1', '', '6');
INSERT INTO `log` VALUES ('400', '2017-04-27 15:37:10.170124', '::1', '1', '', '6');
INSERT INTO `log` VALUES ('401', '2017-04-27 15:44:35.934768', '::1', '1', '', '6');
INSERT INTO `log` VALUES ('402', '2017-04-27 15:48:38.143680', '::1', '1', '', '6');
INSERT INTO `log` VALUES ('403', '2017-04-27 15:55:17.276677', '::1', '1', '', '6');
INSERT INTO `log` VALUES ('404', '2017-04-27 16:01:07.009716', '::1', '3', '新增产品类型 安装包（本地）', '6');
INSERT INTO `log` VALUES ('405', '2017-04-27 16:01:18.202283', '::1', '3', '新增产品类型 安装包 - 本地', '6');
INSERT INTO `log` VALUES ('406', '2017-04-27 16:01:29.151668', '::1', '6', '删除产品类型 安装包（本地）', '6');
INSERT INTO `log` VALUES ('407', '2017-04-27 16:01:34.020894', '::1', '6', '删除产品类型 安装包 - 本地', '6');
INSERT INTO `log` VALUES ('408', '2017-04-27 16:05:18.199832', '::1', '1', '', '6');
INSERT INTO `log` VALUES ('409', '2017-04-27 16:05:41.995771', '::1', '4', '修改产品类型 安装包（本地）', '6');
INSERT INTO `log` VALUES ('410', '2017-04-27 16:08:36.781796', '::1', '1', '', '6');
INSERT INTO `log` VALUES ('411', '2017-04-27 16:08:50.881217', '::1', '4', '修改产品类型 安装包（本地）', '6');
INSERT INTO `log` VALUES ('412', '2017-04-27 16:09:03.541793', '::1', '4', '修改产品类型 安装包 - 本地', '6');
INSERT INTO `log` VALUES ('413', '2017-04-27 16:50:36.920590', '::1', '1', '', '6');
INSERT INTO `log` VALUES ('414', '2017-04-28 08:48:09.032280', '::1', '1', '', '6');
INSERT INTO `log` VALUES ('415', '2017-04-28 10:42:02.856387', '::1', '1', '', '6');
INSERT INTO `log` VALUES ('416', '2017-04-28 10:46:31.994406', '::1', '1', '', '6');
INSERT INTO `log` VALUES ('417', '2017-04-28 10:54:13.813512', '::1', '1', '', '6');
INSERT INTO `log` VALUES ('418', '2017-04-28 10:55:57.253856', '::1', '1', '', '6');
INSERT INTO `log` VALUES ('419', '2017-04-28 10:56:24.223214', '::1', '4', '修改角色信息顶级权限 roleid:3,menuid:20,disable:0', '6');
INSERT INTO `log` VALUES ('420', '2017-04-28 10:59:18.134078', '::1', '1', '', '6');
INSERT INTO `log` VALUES ('421', '2017-04-28 12:03:26.896273', '::1', '1', '', '6');
INSERT INTO `log` VALUES ('422', '2017-04-28 12:03:47.254916', '::1', '4', '重置角色[员工]所有权限，新建并关闭所有权限', '6');
INSERT INTO `log` VALUES ('423', '2017-04-28 12:06:27.843731', '::1', '4', '重置角色[游客]所有权限，新建并关闭所有权限', '6');
INSERT INTO `log` VALUES ('424', '2017-04-28 12:17:50.317832', '::1', '1', '', '6');
INSERT INTO `log` VALUES ('425', '2017-04-28 12:18:37.838041', '::1', '4', '重置角色[游客]所有权限，新建并关闭所有权限', '6');
INSERT INTO `log` VALUES ('426', '2017-04-28 12:22:27.717126', '::1', '1', '', '6');
INSERT INTO `log` VALUES ('427', '2017-04-28 12:30:13.091415', '::1', '1', '', '6');
INSERT INTO `log` VALUES ('428', '2017-04-28 12:30:37.819043', '::1', '4', '修改角色信息顶级权限 roleid:3,menuid:2,disable:1', '6');
INSERT INTO `log` VALUES ('429', '2017-04-28 12:30:48.199950', '::1', '4', '修改角色信息顶级权限 roleid:3,menuid:30,disable:0', '6');
INSERT INTO `log` VALUES ('430', '2017-04-28 12:30:51.467175', '::1', '4', '修改角色信息顶级权限 roleid:3,menuid:15,disable:0', '6');
INSERT INTO `log` VALUES ('431', '2017-04-28 12:33:24.630587', '::1', '1', '', '6');
INSERT INTO `log` VALUES ('432', '2017-04-28 12:33:39.047247', '::1', '4', '修改角色信息顶级权限 roleid:3,menuid:2,disable:1', '6');
INSERT INTO `log` VALUES ('433', '2017-04-28 12:33:42.955843', '::1', '4', '修改角色信息顶级权限 roleid:3,menuid:15,disable:0', '6');
INSERT INTO `log` VALUES ('434', '2017-04-28 12:33:46.365116', '::1', '4', '修改角色信息顶级权限 roleid:3,menuid:30,disable:0', '6');
INSERT INTO `log` VALUES ('435', '2017-04-28 12:33:51.930840', '::1', '4', '修改角色信息顶级权限 roleid:3,menuid:1,disable:1', '6');
INSERT INTO `log` VALUES ('436', '2017-04-28 12:33:58.892488', '::1', '4', '修改角色信息顶级权限 roleid:3,menuid:26,disable:0', '6');
INSERT INTO `log` VALUES ('437', '2017-04-28 12:34:13.093132', '::1', '4', '修改角色信息顶级权限 roleid:3,menuid:7,disable:0', '6');
INSERT INTO `log` VALUES ('438', '2017-04-28 12:34:28.997884', '::1', '4', '修改角色信息顶级权限 roleid:2,menuid:2,disable:1', '6');
INSERT INTO `log` VALUES ('439', '2017-04-28 12:34:32.663335', '::1', '4', '修改角色信息顶级权限 roleid:2,menuid:1,disable:1', '6');
INSERT INTO `log` VALUES ('440', '2017-04-28 12:34:37.128321', '::1', '4', '修改角色信息顶级权限 roleid:2,menuid:3,disable:1', '6');
INSERT INTO `log` VALUES ('441', '2017-04-28 12:34:42.548964', '::1', '4', '修改角色信息顶级权限 roleid:2,menuid:26,disable:0', '6');
INSERT INTO `log` VALUES ('442', '2017-04-28 12:34:48.764147', '::1', '4', '修改角色信息顶级权限 roleid:2,menuid:20,disable:0', '6');
INSERT INTO `log` VALUES ('443', '2017-04-28 13:29:54.682277', '::1', '1', '', '6');
INSERT INTO `log` VALUES ('444', '2017-04-28 13:46:50.870659', '::1', '1', '', '6');
INSERT INTO `log` VALUES ('445', '2017-04-28 13:47:38.463371', '::1', '4', '修改文章类型 其他', '6');
INSERT INTO `log` VALUES ('446', '2017-04-28 13:52:50.803931', '::1', '1', '', '6');
INSERT INTO `log` VALUES ('447', '2017-04-28 14:02:36.656724', '::1', '1', '', '6');
INSERT INTO `log` VALUES ('448', '2017-04-28 14:24:26.923553', '::1', '1', '', '6');
INSERT INTO `log` VALUES ('449', '2017-04-28 14:27:29.423598', '::1', '1', '', '6');
INSERT INTO `log` VALUES ('450', '2017-04-28 14:34:10.091151', '::1', '1', '', '6');
INSERT INTO `log` VALUES ('451', '2017-04-28 14:34:45.275659', '::1', '2', '', '6');
INSERT INTO `log` VALUES ('452', '2017-04-28 14:34:49.215297', '::1', '1', '游客按钮直接登录', '1');
INSERT INTO `log` VALUES ('453', '2017-04-28 14:43:52.226836', '::1', '1', '游客按钮直接登录', '1');
INSERT INTO `log` VALUES ('454', '2017-04-28 14:45:12.099663', '::1', '1', '游客按钮直接登录', '1');
INSERT INTO `log` VALUES ('455', '2017-04-28 14:45:28.875884', '::1', '1', '', '6');
INSERT INTO `log` VALUES ('456', '2017-04-28 14:47:37.884537', '::1', '1', '', '6');
INSERT INTO `log` VALUES ('457', '2017-04-28 14:47:53.901445', '::1', '3', '新增文章:123', '6');
INSERT INTO `log` VALUES ('458', '2017-04-28 14:51:02.221685', '::1', '1', '', '6');
INSERT INTO `log` VALUES ('459', '2017-04-28 15:59:50.715724', '::1', '1', '', '6');
INSERT INTO `log` VALUES ('460', '2017-04-28 16:03:01.003399', '::1', '4', '修改参数[audit]为:1', '6');
INSERT INTO `log` VALUES ('461', '2017-04-28 16:03:03.572188', '::1', '4', '修改参数[audit]为:0', '6');
INSERT INTO `log` VALUES ('462', '2017-04-28 16:26:06.489984', '::1', '1', '', '6');
INSERT INTO `log` VALUES ('463', '2017-04-28 16:26:28.733071', '::1', '4', '重置角色[游客]所有权限，新建并关闭所有权限', '6');
INSERT INTO `log` VALUES ('464', '2017-04-28 16:26:39.326223', '::1', '4', '修改角色信息顶级权限 roleid:3,menuid:2,disable:1', '6');
INSERT INTO `log` VALUES ('465', '2017-04-28 16:26:42.608414', '::1', '4', '修改角色信息顶级权限 roleid:3,menuid:15,disable:0', '6');
INSERT INTO `log` VALUES ('466', '2017-04-28 16:26:47.526752', '::1', '4', '修改角色信息顶级权限 roleid:3,menuid:30,disable:0', '6');
INSERT INTO `log` VALUES ('467', '2017-04-28 16:26:54.316371', '::1', '4', '修改角色信息顶级权限 roleid:3,menuid:1,disable:1', '6');
INSERT INTO `log` VALUES ('468', '2017-04-28 16:27:01.679385', '::1', '4', '修改角色信息顶级权限 roleid:3,menuid:7,disable:0', '6');
INSERT INTO `log` VALUES ('469', '2017-04-28 16:27:06.037338', '::1', '4', '修改角色信息顶级权限 roleid:3,menuid:26,disable:0', '6');
INSERT INTO `log` VALUES ('470', '2017-04-28 16:29:23.506826', '::1', '1', '', '6');
INSERT INTO `log` VALUES ('471', '2017-04-28 16:34:37.080060', '::1', '1', '', '6');
INSERT INTO `log` VALUES ('472', '2017-04-28 16:34:47.589127', '::1', '4', '重置角色[管理员]所有权限，新建并开启所有权限', '6');
INSERT INTO `log` VALUES ('473', '2017-04-28 16:35:10.182353', '::1', '4', '重置角色[游客]所有权限，新建并关闭所有权限', '6');
INSERT INTO `log` VALUES ('474', '2017-04-28 16:35:23.759487', '::1', '4', '修改角色信息顶级权限 roleid:3,menuid:2,disable:1', '6');
INSERT INTO `log` VALUES ('475', '2017-04-28 16:35:28.224475', '::1', '4', '修改角色信息顶级权限 roleid:3,menuid:1,disable:1', '6');
INSERT INTO `log` VALUES ('476', '2017-04-28 16:35:34.140466', '::1', '4', '修改角色信息顶级权限 roleid:3,menuid:7,disable:0', '6');
INSERT INTO `log` VALUES ('477', '2017-04-28 16:35:37.071415', '::1', '4', '修改角色信息顶级权限 roleid:3,menuid:26,disable:0', '6');
INSERT INTO `log` VALUES ('478', '2017-04-28 16:35:39.213833', '::1', '4', '修改角色信息顶级权限 roleid:3,menuid:15,disable:0', '6');
INSERT INTO `log` VALUES ('479', '2017-04-28 16:35:41.549395', '::1', '4', '修改角色信息顶级权限 roleid:3,menuid:30,disable:0', '6');
INSERT INTO `log` VALUES ('480', '2017-04-29 08:01:30.163368', '::1', '1', '', '6');
INSERT INTO `log` VALUES ('481', '2017-04-29 10:43:18.035632', '::1', '1', '', '6');
INSERT INTO `log` VALUES ('482', '2017-04-29 12:28:09.977374', '::1', '1', '', '6');
INSERT INTO `log` VALUES ('483', '2017-04-29 12:31:37.150092', '::1', '1', '', '6');
INSERT INTO `log` VALUES ('484', '2017-04-29 12:32:02.900953', '::1', '1', '', '6');
INSERT INTO `log` VALUES ('485', '2017-04-29 12:32:50.220677', '::1', '1', '', '6');
INSERT INTO `log` VALUES ('486', '2017-04-29 12:33:07.418922', '::1', '1', '', '6');
INSERT INTO `log` VALUES ('487', '2017-04-29 12:53:29.870276', '::1', '1', '', '6');
INSERT INTO `log` VALUES ('488', '2017-04-29 12:53:43.534446', '::1', '1', '游客按钮直接登录', '1');
INSERT INTO `log` VALUES ('489', '2017-04-29 13:07:28.043026', '::1', '1', '', '6');
INSERT INTO `log` VALUES ('490', '2017-04-29 13:07:40.561228', '::1', '1', '', '6');
INSERT INTO `log` VALUES ('491', '2017-04-29 13:08:58.370150', '::1', '1', '', '6');
INSERT INTO `log` VALUES ('492', '2017-04-29 13:12:59.456618', '::1', '1', '', '6');
INSERT INTO `log` VALUES ('493', '2017-04-29 13:13:04.212624', '::1', '1', '', '6');
INSERT INTO `log` VALUES ('494', '2017-04-29 13:13:15.805497', '::1', '1', '', '6');
INSERT INTO `log` VALUES ('495', '2017-04-29 13:13:40.835880', '::1', '1', '', '6');
INSERT INTO `log` VALUES ('496', '2017-04-29 13:13:46.682848', '::1', '1', '', '6');
INSERT INTO `log` VALUES ('497', '2017-04-29 13:19:32.916841', '::1', '1', '', '6');
INSERT INTO `log` VALUES ('498', '2017-04-29 13:19:43.443840', '::1', '1', '', '6');
INSERT INTO `log` VALUES ('499', '2017-04-29 13:24:12.989652', '::1', '1', '', '6');
INSERT INTO `log` VALUES ('500', '2017-04-29 13:24:20.153734', '::1', '1', '', '6');
INSERT INTO `log` VALUES ('501', '2017-04-29 13:24:24.430530', '::1', '1', '', '6');
INSERT INTO `log` VALUES ('502', '2017-04-29 13:24:28.851547', '::1', '1', '', '6');
INSERT INTO `log` VALUES ('503', '2017-04-29 13:27:38.362434', '::1', '1', '', '6');
INSERT INTO `log` VALUES ('504', '2017-04-29 13:31:52.340743', '::1', '1', '', '6');
INSERT INTO `log` VALUES ('505', '2017-04-29 13:32:26.271200', '192.168.0.211', '1', '', '6');
INSERT INTO `log` VALUES ('506', '2017-04-29 13:32:32.034452', '::1', '1', '', '6');
INSERT INTO `log` VALUES ('507', '2017-04-29 13:32:57.920647', '192.168.0.211', '1', '', '6');
INSERT INTO `log` VALUES ('508', '2017-04-29 13:33:11.333453', '::1', '1', '', '6');
INSERT INTO `log` VALUES ('509', '2017-04-29 13:33:51.091463', '192.168.0.211', '1', '', '6');
INSERT INTO `log` VALUES ('510', '2017-04-29 13:35:40.711191', '192.168.0.211', '1', '', '6');
INSERT INTO `log` VALUES ('511', '2017-04-29 13:44:46.664588', '::1', '1', '', '6');
INSERT INTO `log` VALUES ('512', '2017-04-29 13:45:08.003926', '::1', '2', '', '6');
INSERT INTO `log` VALUES ('513', '2017-04-29 13:45:10.899746', '::1', '1', '', '6');
INSERT INTO `log` VALUES ('514', '2017-04-29 13:48:21.443356', '::1', '1', '', '6');
INSERT INTO `log` VALUES ('515', '2017-04-29 14:04:31.759791', '::1', '1', '', '6');
INSERT INTO `log` VALUES ('516', '2017-04-29 14:20:58.961610', '::1', '1', '', '6');
INSERT INTO `log` VALUES ('517', '2017-04-29 14:35:33.393483', '::1', '1', '', '6');
INSERT INTO `log` VALUES ('571', '2017-05-02 12:24:43.556808', '::1', '1', '游客按钮直接登录', '1');
INSERT INTO `log` VALUES ('572', '2017-05-02 12:31:23.500508', '::1', '1', '', '6');
INSERT INTO `log` VALUES ('573', '2017-05-02 12:39:48.830186', '::1', '1', '游客按钮直接登录', '1');
INSERT INTO `log` VALUES ('574', '2017-05-02 12:40:25.446927', '::1', '2', '', '1');
INSERT INTO `log` VALUES ('575', '2017-05-02 12:40:28.760149', '::1', '1', '', '6');
INSERT INTO `log` VALUES ('576', '2017-05-02 12:41:13.909706', '::1', '2', '', '6');
INSERT INTO `log` VALUES ('577', '2017-05-02 12:41:16.628535', '::1', '1', '游客按钮直接登录', '1');
INSERT INTO `log` VALUES ('578', '2017-05-02 12:43:58.062923', '::1', '1', '', '6');
INSERT INTO `log` VALUES ('579', '2017-05-02 12:44:18.809941', '::1', '2', '', '6');
INSERT INTO `log` VALUES ('580', '2017-05-02 12:44:21.304597', '::1', '1', '游客按钮直接登录', '1');
INSERT INTO `log` VALUES ('581', '2017-05-02 12:50:01.995103', '::1', '1', '游客按钮直接登录', '1');
INSERT INTO `log` VALUES ('582', '2017-05-02 12:58:32.837088', '::1', '1', '游客按钮直接登录', '1');
INSERT INTO `log` VALUES ('583', '2017-05-02 12:59:14.940731', '::1', '1', '游客按钮直接登录', '1');
INSERT INTO `log` VALUES ('584', '2017-05-02 13:00:06.629758', '::1', '2', '', '1');
INSERT INTO `log` VALUES ('585', '2017-05-02 13:00:10.552361', '::1', '1', '', '6');
INSERT INTO `log` VALUES ('586', '2017-05-02 13:05:45.427163', '::1', '1', '', '6');
INSERT INTO `log` VALUES ('587', '2017-05-02 13:06:27.063803', '::1', '4', '修改用户 测试', '6');
INSERT INTO `log` VALUES ('588', '2017-05-02 13:06:59.232157', '::1', '2', '', '6');
INSERT INTO `log` VALUES ('589', '2017-05-02 13:07:02.549359', '::1', '1', '游客按钮直接登录', '1');
INSERT INTO `log` VALUES ('590', '2017-05-02 13:09:26.890937', '::1', '2', '', '1');
INSERT INTO `log` VALUES ('591', '2017-05-02 13:09:28.723175', '::1', '1', '', '6');
INSERT INTO `log` VALUES ('592', '2017-05-02 14:40:16.597280', '::1', '1', '', '6');
INSERT INTO `log` VALUES ('593', '2017-05-02 15:08:45.820873', '::1', '1', '', '6');
INSERT INTO `log` VALUES ('594', '2017-05-02 15:09:01.256119', '::1', '2', '', '6');
INSERT INTO `log` VALUES ('595', '2017-05-02 15:09:06.822814', '::1', '1', '', '6');
INSERT INTO `log` VALUES ('596', '2017-05-02 15:10:42.774152', '::1', '1', '', '6');
INSERT INTO `log` VALUES ('597', '2017-05-02 15:10:49.129379', '::1', '2', '', '6');
INSERT INTO `log` VALUES ('598', '2017-05-02 15:11:07.558080', '::1', '1', '', '6');
INSERT INTO `log` VALUES ('599', '2017-05-02 15:15:28.504793', '127.0.0.1', '1', '', '6');
INSERT INTO `log` VALUES ('600', '2017-05-02 15:23:24.437168', '::1', '1', '', '6');
INSERT INTO `log` VALUES ('601', '2017-05-02 16:03:16.562543', '::1', '1', '', '6');
INSERT INTO `log` VALUES ('602', '2017-05-02 16:08:15.129865', '::1', '1', '', '6');
INSERT INTO `log` VALUES ('603', '2017-05-02 16:08:31.091618', '::1', '4', '重置角色[管理员]所有权限，新建并开启所有权限', '6');
INSERT INTO `log` VALUES ('604', '2017-05-03 08:25:37.914355', '::1', '1', '', '6');
INSERT INTO `log` VALUES ('605', '2017-05-03 08:29:59.806727', '::1', '1', '', '6');
INSERT INTO `log` VALUES ('606', '2017-05-03 08:45:11.739436', '::1', '1', '', '6');
INSERT INTO `log` VALUES ('607', '2017-05-03 08:45:54.657700', '::1', '1', '', '6');
INSERT INTO `log` VALUES ('608', '2017-05-03 09:36:45.151419', '::1', '1', '', '6');
INSERT INTO `log` VALUES ('609', '2017-05-03 09:37:36.982694', '::1', '2', '', '6');
INSERT INTO `log` VALUES ('610', '2017-05-03 10:43:35.595461', '::1', '1', '', '6');
INSERT INTO `log` VALUES ('611', '2017-05-03 10:43:45.780369', '::1', '2', '', '6');

-- ----------------------------
-- Table structure for `menu`
-- ----------------------------
DROP TABLE IF EXISTS `menu`;
CREATE TABLE `menu` (
  `Id` int(11) NOT NULL AUTO_INCREMENT,
  `Createtime` datetime(6) NOT NULL,
  `Disable` int(11) NOT NULL,
  `Disabledesc` longtext,
  `Menu_idname` longtext,
  `Menu_name` longtext,
  `Menu_rank` int(11) NOT NULL,
  `Menu_target` longtext,
  `Menu_url` longtext,
  `Parent_Id` int(11) NOT NULL COMMENT '父级ID',
  `Root_Id` int(11) DEFAULT NULL COMMENT '顶级ID',
  PRIMARY KEY (`Id`)
) ENGINE=InnoDB AUTO_INCREMENT=31 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of menu
-- ----------------------------
INSERT INTO `menu` VALUES ('1', '2017-03-25 08:25:45.000000', '1', null, null, '金帆论坛', '2', null, null, '0', '0');
INSERT INTO `menu` VALUES ('2', '2017-03-25 08:26:12.000000', '1', null, null, '产品中心', '1', null, null, '0', '0');
INSERT INTO `menu` VALUES ('3', '2017-03-25 08:26:31.000000', '1', null, null, '用户中心', '3', null, null, '0', '0');
INSERT INTO `menu` VALUES ('4', '2017-03-25 08:27:11.000000', '1', null, 'WikiMain', '论坛首页', '1', 'navtab', 'Wiki/Main', '23', '1');
INSERT INTO `menu` VALUES ('5', '2017-03-25 08:27:28.000000', '1', null, 'WikiList', '文章列表', '2', 'navtab', 'Wiki/List', '23', '1');
INSERT INTO `menu` VALUES ('6', '2017-03-25 08:27:50.000000', '0', null, null, '提问列表', '3', 'navtab', null, '23', '1');
INSERT INTO `menu` VALUES ('7', '2017-03-25 08:28:23.000000', '1', null, null, '我的空间', '2', null, null, '1', '1');
INSERT INTO `menu` VALUES ('8', '2017-03-25 08:29:08.000000', '0', null, 'Wikiindex', '最新动态', '1', 'navtab', 'Wiki/index', '7', '1');
INSERT INTO `menu` VALUES ('9', '2017-03-25 08:29:39.000000', '1', null, 'WikiEdit', '发布文章', '2', 'navtab', 'Wiki/Edit', '7', '1');
INSERT INTO `menu` VALUES ('10', '2017-03-25 08:30:07.000000', '0', null, null, '发布提问', '3', 'navtab', null, '7', '1');
INSERT INTO `menu` VALUES ('11', '2017-03-25 08:30:40.000000', '1', null, 'WikiMylist', '我的文章', '4', 'navtab', 'Wiki/Mylist', '7', '1');
INSERT INTO `menu` VALUES ('12', '2017-03-25 08:30:59.000000', '0', null, null, '我的提问', '5', 'navtab', null, '7', '1');
INSERT INTO `menu` VALUES ('13', '2017-03-25 08:31:15.000000', '1', null, null, '软件中心', '1', null, null, '2', '2');
INSERT INTO `menu` VALUES ('14', '2017-03-25 08:31:59.000000', '1', null, 'ProductList', '软件列表', '1', 'navtab', 'Product/List', '13', '2');
INSERT INTO `menu` VALUES ('15', '2017-03-25 08:32:21.000000', '1', null, 'ProductIndex', '软件管理', '3', 'navtab', 'Product/Index', '13', '2');
INSERT INTO `menu` VALUES ('16', '2017-03-25 08:33:01.000000', '1', null, null, '个人中心', '1', null, null, '3', '3');
INSERT INTO `menu` VALUES ('17', '2017-03-25 08:33:29.000000', '0', null, 'UsersMyinfo', '我的信息', '1', 'navtab', null, '16', '3');
INSERT INTO `menu` VALUES ('18', '2017-03-25 08:33:43.000000', '1', null, 'UsersPassword', '修改密码', '2', 'dialog', 'Users/Password', '16', '3');
INSERT INTO `menu` VALUES ('19', '2017-04-01 08:33:59.000000', '1', null, 'UsersMyloglist', '个人记录', '3', 'navtab', 'Users/Myloglist', '16', '3');
INSERT INTO `menu` VALUES ('20', '2017-03-25 08:34:17.000000', '1', null, null, '用户中心', '2', null, null, '3', '3');
INSERT INTO `menu` VALUES ('21', '2017-03-25 08:34:39.000000', '1', null, 'UsersIndex', '用户管理', '3', 'navtab', 'Users/Index', '20', '3');
INSERT INTO `menu` VALUES ('22', '2017-03-25 08:34:54.000000', '1', null, 'UsersLoglist', '平台记录', '4', 'navtab', 'Users/Loglist', '20', '3');
INSERT INTO `menu` VALUES ('23', '2017-03-25 08:41:09.000000', '1', null, null, '金帆论坛', '1', null, null, '1', '1');
INSERT INTO `menu` VALUES ('24', '2017-03-25 08:44:58.000000', '1', null, 'UsersMenu', '菜单管理', '2', 'navtab', 'Users/Menu', '20', '3');
INSERT INTO `menu` VALUES ('25', '2017-03-25 09:31:02.000000', '1', null, 'UsersRole', '角色管理', '1', 'navtab', 'Users/Role', '20', '3');
INSERT INTO `menu` VALUES ('26', '2017-03-28 10:29:39.000000', '1', null, null, '论坛管理', '3', null, null, '1', '1');
INSERT INTO `menu` VALUES ('27', '2017-03-28 10:30:16.000000', '1', null, 'WikiArticleType', '类型管理', '1', null, 'Wiki/ArticleType', '26', '1');
INSERT INTO `menu` VALUES ('28', '2017-03-28 10:30:37.000000', '1', null, 'WikiAdminList', '审核管理', '2', 'navtab', 'Wiki/AdminList', '26', '1');
INSERT INTO `menu` VALUES ('29', '2017-03-28 10:30:55.000000', '1', null, 'WikiConfigs', '参数设置', '3', null, 'Wiki/Configs', '26', '1');
INSERT INTO `menu` VALUES ('30', '2017-03-31 12:18:40.000000', '1', null, 'ProductType', '类型管理', '2', 'navtab', 'Product/Type', '13', '2');

-- ----------------------------
-- Table structure for `operation`
-- ----------------------------
DROP TABLE IF EXISTS `operation`;
CREATE TABLE `operation` (
  `Id` int(11) NOT NULL AUTO_INCREMENT,
  `Operation` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`Id`)
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of operation
-- ----------------------------
INSERT INTO `operation` VALUES ('1', '登录');
INSERT INTO `operation` VALUES ('2', '登出');
INSERT INTO `operation` VALUES ('3', '新增');
INSERT INTO `operation` VALUES ('4', '修改');
INSERT INTO `operation` VALUES ('5', '查询');
INSERT INTO `operation` VALUES ('6', '删除');
INSERT INTO `operation` VALUES ('7', '上传');
INSERT INTO `operation` VALUES ('8', '下载');
INSERT INTO `operation` VALUES ('9', '审核');
INSERT INTO `operation` VALUES ('10', '驳回');

-- ----------------------------
-- Table structure for `power`
-- ----------------------------
DROP TABLE IF EXISTS `power`;
CREATE TABLE `power` (
  `Id` int(11) NOT NULL AUTO_INCREMENT,
  `TypeName` varchar(255) DEFAULT NULL,
  `PowerName` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`Id`)
) ENGINE=InnoDB AUTO_INCREMENT=23 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of power
-- ----------------------------
INSERT INTO `power` VALUES ('1', 'WIki', '新增');
INSERT INTO `power` VALUES ('2', 'Wiki', '回复');
INSERT INTO `power` VALUES ('3', 'Wiki', '修改');
INSERT INTO `power` VALUES ('4', 'Wiki', '删除');
INSERT INTO `power` VALUES ('5', 'Wiki', '审核');
INSERT INTO `power` VALUES ('6', 'Wiki', '配置');
INSERT INTO `power` VALUES ('7', 'Wiki', '类型新增');
INSERT INTO `power` VALUES ('8', 'Wiki', '类型删除');
INSERT INTO `power` VALUES ('9', 'Wiki', '类型修改');
INSERT INTO `power` VALUES ('10', '软件', '新增');
INSERT INTO `power` VALUES ('11', '软件', '删除');
INSERT INTO `power` VALUES ('12', '软件', '修改');
INSERT INTO `power` VALUES ('13', '软件类型', '类型新增');
INSERT INTO `power` VALUES ('14', '软件类型', '类型删除');
INSERT INTO `power` VALUES ('15', '软件类型', '类型修改');
INSERT INTO `power` VALUES ('16', '用户', '新增');
INSERT INTO `power` VALUES ('17', '用户', '修改');
INSERT INTO `power` VALUES ('18', '用户', '删除');
INSERT INTO `power` VALUES ('19', '角色', '新增');
INSERT INTO `power` VALUES ('20', '角色', '修改');
INSERT INTO `power` VALUES ('21', '角色', '删除');
INSERT INTO `power` VALUES ('22', '菜单', '修改');

-- ----------------------------
-- Table structure for `product`
-- ----------------------------
DROP TABLE IF EXISTS `product`;
CREATE TABLE `product` (
  `Id` int(11) NOT NULL AUTO_INCREMENT,
  `Createtime` datetime(6) NOT NULL,
  `Disable` int(11) NOT NULL,
  `Disabledesc` longtext,
  `ProductType_Id` int(11) NOT NULL,
  `Product_No` longtext,
  `Product_name` longtext,
  `Product_txt` longtext,
  `Product_url` longtext,
  `User_Id` int(11) NOT NULL,
  PRIMARY KEY (`Id`),
  KEY `product_typeid` (`ProductType_Id`),
  KEY `product_userid` (`User_Id`),
  CONSTRAINT `product_typeid` FOREIGN KEY (`ProductType_Id`) REFERENCES `producttype` (`Id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `product_userid` FOREIGN KEY (`User_Id`) REFERENCES `users` (`Id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=65 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of product
-- ----------------------------
INSERT INTO `product` VALUES ('4', '2017-03-30 20:59:38.803372', '1', null, '3', 'No_009134', '测试软件名称', null, 'test36.rar', '2');
INSERT INTO `product` VALUES ('5', '2017-03-18 14:35:52.230269', '1', null, '3', 'NO_0000123', '下载测试', null, '新建文本文档.rar', '2');
INSERT INTO `product` VALUES ('6', '2017-03-23 08:38:51.792095', '1', null, '3', '111', '1115555', null, '新建文本文档.rar', '2');
INSERT INTO `product` VALUES ('7', '2017-03-20 10:05:04.189533', '1', 'string', '3', 'string', 'string', 'string', '新建文本文档.rar', '2');
INSERT INTO `product` VALUES ('8', '2017-03-20 10:16:25.319174', '1', 'string', '3', 'string', 'string', 'string', '新建文本文档.rar', '2');
INSERT INTO `product` VALUES ('9', '2017-03-30 21:08:28.545548', '1', 'string', '3', 'string', 'string', 'string', 'test36.rar', '2');
INSERT INTO `product` VALUES ('10', '2017-03-30 20:59:10.940002', '1', 'string', '3', 'string', '测试', 'string', 'test6.rar', '2');
INSERT INTO `product` VALUES ('13', '2017-04-05 10:40:10.933403', '1', 'string', '3', 'post测试No', 'post测试Name', 'post测试txt', 'post测试url', '2');
INSERT INTO `product` VALUES ('14', '2017-04-05 23:19:00.866347', '1', null, '1', '软件编号', '软件名称bbbbb', '软件描述', '下载地址', '2');
INSERT INTO `product` VALUES ('15', '2017-04-05 23:21:02.733871', '1', null, '1', '软件编号', '软件名称aaab', '软件描述', '下载地址', '2');
INSERT INTO `product` VALUES ('16', '2017-04-06 01:50:31.828648', '1', null, '1', 'bb', 'aa', 'aa Vbb', 'cc', '2');
INSERT INTO `product` VALUES ('17', '2017-04-06 03:00:59.545296', '1', null, '1', '6.0.4.1', '锂电化成', '锂电化成 V6.0.4.1', '锂电化成 V6.0.4.1.rar', '2');
INSERT INTO `product` VALUES ('18', '2017-04-06 03:01:00.019790', '1', null, '2', '6.0.4.1', '锂电化成Map', '锂电化成Map V6.0.4.1', '锂电化成 V6.0.4.1.rar', '2');
INSERT INTO `product` VALUES ('19', '2017-04-06 03:01:00.461983', '1', null, '1', '6.0.2.2', '锂电检测', '锂电检测 V6.0.2.2', '锂电检测 V6.0.2.2.rar', '2');
INSERT INTO `product` VALUES ('20', '2017-04-06 03:01:00.871568', '1', null, '2', '6.0.2.2', '锂电检测Map', '锂电检测Map V6.0.2.2', '锂电检测 V6.0.2.2.rar', '2');
INSERT INTO `product` VALUES ('21', '2017-04-06 03:01:01.279678', '1', null, '1', '6.0.3.2', '铅酸化成', '铅酸化成 V6.0.3.2', '铅酸化成 V6.0.3.2.rar', '2');
INSERT INTO `product` VALUES ('22', '2017-04-06 03:01:01.686432', '1', null, '2', '6.0.3.2', '铅酸化成Map', '铅酸化成Map V6.0.3.2', '铅酸化成 V6.0.3.2.rar', '2');
INSERT INTO `product` VALUES ('23', '2017-04-06 03:01:02.097704', '1', null, '1', '2.0.4.4', 'OCVIR', 'OCVIR V2.0.4.4', 'OCVIR V2.0.4.4.rar', '2');
INSERT INTO `product` VALUES ('24', '2017-04-06 03:01:02.495083', '1', null, '2', '2.0.4.4', 'OCVIRMap', 'OCVIRMap V2.0.4.4', 'OCVIR V2.0.4.4.rar', '2');
INSERT INTO `product` VALUES ('25', '2017-04-06 03:01:02.909985', '1', null, '1', '6.0.2.1', '数据分析', '数据分析 V6.0.2.1', '数据分析 V6.0.2.1.rar', '2');
INSERT INTO `product` VALUES ('26', '2017-04-06 03:01:03.308205', '1', null, '2', '6.0.2.1', '数据分析x86Map', '数据分析x86Map V6.0.2.1', '数据分析 V6.0.2.1.rar', '2');
INSERT INTO `product` VALUES ('27', '2017-04-06 03:01:03.706641', '1', null, '2', '6.0.2.1', '数据分析x64Map', '数据分析x64Map V6.0.2.1', '数据分析 V6.0.2.1.rar', '2');
INSERT INTO `product` VALUES ('28', '2017-04-06 03:01:04.114032', '1', null, '1', '6.0.4.2', '数据中心开发', '数据中心开发 V6.0.4.2', '数据中心开发 V6.0.4.2.rar', '2');
INSERT INTO `product` VALUES ('29', '2017-04-06 03:01:04.507420', '1', null, '2', '6.0.4.2', '数据中心开发Map', '数据中心开发Map V6.0.4.2', '数据中心开发 V6.0.4.2.rar', '2');
INSERT INTO `product` VALUES ('30', '2017-04-06 03:01:04.891832', '1', null, '1', '6.0.4.2', '信息系统', '信息系统 V6.0.4.2', '信息系统 V6.0.4.2.rar', '2');
INSERT INTO `product` VALUES ('31', '2017-04-06 03:01:05.324857', '1', null, '1', '2.0.2.1', '电池登录', '电池登录 V2.0.2.1', '电池登录 V2.0.2.1.rar', '2');
INSERT INTO `product` VALUES ('32', '2017-04-06 03:01:05.720071', '1', null, '2', '2.0.2.1', '电池登录Map', '电池登录Map V2.0.2.1', '电池登录 V2.0.2.1.rar', '2');
INSERT INTO `product` VALUES ('33', '2017-04-23 21:14:48.645969', '1', null, '1', '6.0.4.2', '锂电化成', '锂电化成 V6.0.4.2', '锂电化成 V6.0.4.2.rar', '2');
INSERT INTO `product` VALUES ('34', '2017-04-23 21:15:04.286812', '1', null, '2', '6.0.4.2', '锂电化成Map', '锂电化成Map V6.0.4.2', '锂电化成 V6.0.4.2.rar', '2');
INSERT INTO `product` VALUES ('35', '2017-04-23 21:15:04.870779', '1', null, '1', '6.0.2.2', '锂电检测', '锂电检测 V6.0.2.2', '锂电检测 V6.0.2.2.rar', '2');
INSERT INTO `product` VALUES ('36', '2017-04-23 21:15:05.320888', '1', null, '2', '6.0.2.2', '锂电检测Map', '锂电检测Map V6.0.2.2', '锂电检测 V6.0.2.2.rar', '2');
INSERT INTO `product` VALUES ('37', '2017-04-23 21:15:05.795065', '1', null, '1', '6.0.3.2', '铅酸化成', '铅酸化成 V6.0.3.2', '铅酸化成 V6.0.3.2.rar', '2');
INSERT INTO `product` VALUES ('38', '2017-04-23 21:15:06.210678', '1', null, '2', '6.0.3.2', '铅酸化成Map', '铅酸化成Map V6.0.3.2', '铅酸化成 V6.0.3.2.rar', '2');
INSERT INTO `product` VALUES ('39', '2017-04-23 21:15:06.640216', '1', null, '1', '2.0.4.6', 'OCVIR', 'OCVIR V2.0.4.6', 'OCVIR V2.0.4.6.rar', '2');
INSERT INTO `product` VALUES ('40', '2017-04-23 21:15:07.068971', '1', null, '2', '2.0.4.6', 'OCVIRMap', 'OCVIRMap V2.0.4.6', 'OCVIR V2.0.4.6.rar', '2');
INSERT INTO `product` VALUES ('41', '2017-04-23 21:15:07.491714', '1', null, '1', '6.0.2.1', '数据分析', '数据分析 V6.0.2.1', '数据分析 V6.0.2.1.rar', '2');
INSERT INTO `product` VALUES ('42', '2017-04-23 21:15:07.924113', '1', null, '2', '6.0.2.1', '数据分析x86Map', '数据分析x86Map V6.0.2.1', '数据分析 V6.0.2.1.rar', '2');
INSERT INTO `product` VALUES ('43', '2017-04-23 21:15:08.367445', '1', null, '2', '6.0.2.1', '数据分析x64Map', '数据分析x64Map V6.0.2.1', '数据分析 V6.0.2.1.rar', '2');
INSERT INTO `product` VALUES ('44', '2017-04-23 21:15:08.804628', '1', null, '1', '6.0.4.2', '数据中心开发', '数据中心开发 V6.0.4.2', '数据中心开发 V6.0.4.2.rar', '2');
INSERT INTO `product` VALUES ('45', '2017-04-23 21:15:09.238375', '1', null, '2', '6.0.4.2', '数据中心开发Map', '数据中心开发Map V6.0.4.2', '数据中心开发 V6.0.4.2.rar', '2');
INSERT INTO `product` VALUES ('46', '2017-04-23 21:15:09.641455', '1', null, '1', '6.0.4.2', '信息系统', '信息系统 V6.0.4.2', '信息系统 V6.0.4.2.rar', '2');
INSERT INTO `product` VALUES ('47', '2017-04-23 21:15:10.098786', '1', null, '1', '2.0.2.1', '电池登录', '电池登录 V2.0.2.1', '电池登录 V2.0.2.1.rar', '2');
INSERT INTO `product` VALUES ('48', '2017-04-23 21:15:10.522583', '1', null, '2', '2.0.2.1', '电池登录Map', '电池登录Map V2.0.2.1', '电池登录 V2.0.2.1.rar', '2');
INSERT INTO `product` VALUES ('49', '2017-04-23 23:31:25.542316', '1', null, '1', '6.0.4.2', '锂电化成', '锂电化成 V6.0.4.2', '锂电化成 V6.0.4.2.rar', '2');
INSERT INTO `product` VALUES ('50', '2017-04-23 23:31:27.042437', '1', null, '2', '6.0.4.2', '锂电化成Map', '锂电化成Map V6.0.4.2', '锂电化成 V6.0.4.2.rar', '2');
INSERT INTO `product` VALUES ('51', '2017-04-23 23:31:28.101127', '1', null, '1', '6.0.2.2', '锂电检测', '锂电检测 V6.0.2.2', '锂电检测 V6.0.2.2.rar', '2');
INSERT INTO `product` VALUES ('52', '2017-04-23 23:31:28.675443', '1', null, '2', '6.0.2.2', '锂电检测Map', '锂电检测Map V6.0.2.2', '锂电检测 V6.0.2.2.rar', '2');
INSERT INTO `product` VALUES ('53', '2017-04-23 23:31:29.223163', '1', null, '1', '6.0.3.2', '铅酸化成', '铅酸化成 V6.0.3.2', '铅酸化成 V6.0.3.2.rar', '2');
INSERT INTO `product` VALUES ('54', '2017-04-23 23:31:29.751647', '1', null, '2', '6.0.3.2', '铅酸化成Map', '铅酸化成Map V6.0.3.2', '铅酸化成 V6.0.3.2.rar', '2');
INSERT INTO `product` VALUES ('55', '2017-04-23 23:31:30.211361', '1', null, '1', '2.0.4.6', 'OCVIR', 'OCVIR V2.0.4.6', 'OCVIR V2.0.4.6.rar', '2');
INSERT INTO `product` VALUES ('56', '2017-04-23 23:31:30.649021', '1', null, '2', '2.0.4.6', 'OCVIRMap', 'OCVIRMap V2.0.4.6', 'OCVIR V2.0.4.6.rar', '2');
INSERT INTO `product` VALUES ('57', '2017-04-23 23:31:31.123485', '1', null, '1', '6.0.2.1', '数据分析', '数据分析 V6.0.2.1', '数据分析 V6.0.2.1.rar', '2');
INSERT INTO `product` VALUES ('58', '2017-04-23 23:31:31.560495', '1', null, '2', '6.0.2.1', '数据分析x86Map', '数据分析x86Map V6.0.2.1', '数据分析 V6.0.2.1.rar', '2');
INSERT INTO `product` VALUES ('59', '2017-04-23 23:31:31.976166', '1', null, '2', '6.0.2.1', '数据分析x64Map', '数据分析x64Map V6.0.2.1', '数据分析 V6.0.2.1.rar', '2');
INSERT INTO `product` VALUES ('60', '2017-04-23 23:31:32.458383', '1', null, '1', '6.0.4.2', '数据中心开发', '数据中心开发 V6.0.4.2', '数据中心开发 V6.0.4.2.rar', '2');
INSERT INTO `product` VALUES ('61', '2017-04-23 23:31:32.891665', '1', null, '2', '6.0.4.2', '数据中心开发Map', '数据中心开发Map V6.0.4.2', '数据中心开发 V6.0.4.2.rar', '2');
INSERT INTO `product` VALUES ('62', '2017-04-23 23:31:33.340715', '1', null, '1', '6.0.4.2', '信息系统', '信息系统 V6.0.4.2', '信息系统 V6.0.4.2.rar', '2');
INSERT INTO `product` VALUES ('63', '2017-04-23 23:31:33.793838', '1', null, '1', '2.0.2.1', '电池登录', '电池登录 V2.0.2.1', '电池登录 V2.0.2.1.rar', '2');
INSERT INTO `product` VALUES ('64', '2017-04-23 23:31:34.187981', '1', null, '2', '2.0.2.1', '电池登录Map', '电池登录Map V2.0.2.1', '电池登录 V2.0.2.1.rar', '2');

-- ----------------------------
-- Table structure for `producttype`
-- ----------------------------
DROP TABLE IF EXISTS `producttype`;
CREATE TABLE `producttype` (
  `Id` int(11) NOT NULL AUTO_INCREMENT,
  `Createtime` datetime(6) NOT NULL,
  `Disable` int(11) NOT NULL,
  `Disabledesc` longtext,
  `ProductType_No` longtext,
  `ProductType_name` longtext,
  `ProductType_txt` longtext,
  `User_Id` int(11) NOT NULL,
  PRIMARY KEY (`Id`),
  KEY `producttype_userid` (`User_Id`),
  CONSTRAINT `producttype_userid` FOREIGN KEY (`User_Id`) REFERENCES `users` (`Id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of producttype
-- ----------------------------
INSERT INTO `producttype` VALUES ('1', '2017-03-30 08:46:02.000000', '0', null, '001', '安装包', '', '2');
INSERT INTO `producttype` VALUES ('2', '2017-03-31 12:49:11.425321', '0', null, null, 'XML文件', null, '2');
INSERT INTO `producttype` VALUES ('3', '2017-04-27 16:09:03.059475', '0', null, null, '安装包 - 本地', null, '2');

-- ----------------------------
-- Table structure for `reply`
-- ----------------------------
DROP TABLE IF EXISTS `reply`;
CREATE TABLE `reply` (
  `Id` int(11) NOT NULL AUTO_INCREMENT,
  `Article_Id` int(11) NOT NULL,
  `Createtime` datetime(6) NOT NULL,
  `Parent_Id` int(11) NOT NULL,
  `Reply_txt` longtext,
  `User_Id` int(11) NOT NULL,
  PRIMARY KEY (`Id`),
  KEY `aid` (`Article_Id`),
  KEY `uid` (`User_Id`),
  CONSTRAINT `aid` FOREIGN KEY (`Article_Id`) REFERENCES `article` (`Id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `uid` FOREIGN KEY (`User_Id`) REFERENCES `users` (`Id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=26 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of reply
-- ----------------------------
INSERT INTO `reply` VALUES ('11', '14', '2017-03-23 15:06:06.686342', '0', '测试回复', '2');
INSERT INTO `reply` VALUES ('12', '14', '2017-03-23 15:06:23.263179', '11', '回复[测试回复]   测试递归回复', '2');
INSERT INTO `reply` VALUES ('13', '14', '2017-03-23 15:06:45.038250', '12', '回复[回复[测试回复]   测试递归回复]   测试递归回复2', '2');
INSERT INTO `reply` VALUES ('14', '14', '2017-03-23 15:06:50.555144', '0', '2141241', '2');
INSERT INTO `reply` VALUES ('15', '14', '2017-03-23 15:09:37.003242', '0', '21312414', '2');
INSERT INTO `reply` VALUES ('16', '14', '2017-03-23 15:14:40.382595', '11', '回复[测试回复]   444444', '1');
INSERT INTO `reply` VALUES ('17', '14', '2017-03-23 15:15:08.460016', '0', 'dddddd', '1');
INSERT INTO `reply` VALUES ('18', '15', '2017-03-23 15:23:38.158730', '0', '测试王企鹅二', '2');
INSERT INTO `reply` VALUES ('19', '15', '2017-03-23 15:24:04.405597', '18', '回复[测试王企鹅二]   回复131 ', '1');
INSERT INTO `reply` VALUES ('20', '14', '2017-03-23 15:33:09.249272', '17', '323545345345', '2');
INSERT INTO `reply` VALUES ('21', '15', '2017-03-23 15:34:12.430258', '19', '35425325', '2');
INSERT INTO `reply` VALUES ('22', '14', '2017-03-23 17:01:19.033172', '17', '53646t\r\n25\r\n25', '2');
INSERT INTO `reply` VALUES ('23', '15', '2017-03-24 08:35:37.774413', '19', '87i7\r\n78ik7\r\n7k', '2');
INSERT INTO `reply` VALUES ('24', '14', '2017-03-24 09:07:29.269827', '17', 'q3r2q3t 9087', '2');
INSERT INTO `reply` VALUES ('25', '5', '2017-03-30 13:16:17.517386', '0', '113366666', '2');

-- ----------------------------
-- Table structure for `role`
-- ----------------------------
DROP TABLE IF EXISTS `role`;
CREATE TABLE `role` (
  `Id` int(11) NOT NULL AUTO_INCREMENT,
  `Createtime` datetime(6) NOT NULL,
  `Disable` int(11) NOT NULL,
  `Disabledesc` longtext,
  `Role_name` longtext,
  PRIMARY KEY (`Id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of role
-- ----------------------------
INSERT INTO `role` VALUES ('1', '2017-03-25 08:47:45.000000', '1', null, '管理员');
INSERT INTO `role` VALUES ('2', '2017-03-25 08:48:03.000000', '1', null, '员工');
INSERT INTO `role` VALUES ('3', '2017-03-25 13:13:32.835426', '1', null, '游客');

-- ----------------------------
-- Table structure for `rolemenu`
-- ----------------------------
DROP TABLE IF EXISTS `rolemenu`;
CREATE TABLE `rolemenu` (
  `Id` int(11) NOT NULL AUTO_INCREMENT,
  `Disable` int(11) NOT NULL,
  `Menu_Id` int(11) NOT NULL,
  `Role_Id` int(11) NOT NULL,
  PRIMARY KEY (`Id`),
  KEY `roleid` (`Role_Id`) USING BTREE,
  KEY `rolemenu_menuid` (`Menu_Id`),
  CONSTRAINT `rolemenu_menuid` FOREIGN KEY (`Menu_Id`) REFERENCES `menu` (`Id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `rolemenu_roleid` FOREIGN KEY (`Role_Id`) REFERENCES `role` (`Id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=685 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of rolemenu
-- ----------------------------
INSERT INTO `rolemenu` VALUES ('468', '1', '1', '2');
INSERT INTO `rolemenu` VALUES ('469', '1', '2', '2');
INSERT INTO `rolemenu` VALUES ('470', '1', '3', '2');
INSERT INTO `rolemenu` VALUES ('471', '1', '4', '2');
INSERT INTO `rolemenu` VALUES ('472', '1', '5', '2');
INSERT INTO `rolemenu` VALUES ('473', '1', '6', '2');
INSERT INTO `rolemenu` VALUES ('474', '1', '7', '2');
INSERT INTO `rolemenu` VALUES ('475', '1', '8', '2');
INSERT INTO `rolemenu` VALUES ('476', '1', '9', '2');
INSERT INTO `rolemenu` VALUES ('477', '1', '10', '2');
INSERT INTO `rolemenu` VALUES ('478', '1', '11', '2');
INSERT INTO `rolemenu` VALUES ('479', '1', '12', '2');
INSERT INTO `rolemenu` VALUES ('480', '1', '13', '2');
INSERT INTO `rolemenu` VALUES ('481', '1', '14', '2');
INSERT INTO `rolemenu` VALUES ('482', '1', '15', '2');
INSERT INTO `rolemenu` VALUES ('483', '1', '16', '2');
INSERT INTO `rolemenu` VALUES ('484', '1', '17', '2');
INSERT INTO `rolemenu` VALUES ('485', '1', '18', '2');
INSERT INTO `rolemenu` VALUES ('486', '1', '19', '2');
INSERT INTO `rolemenu` VALUES ('487', '0', '20', '2');
INSERT INTO `rolemenu` VALUES ('488', '0', '21', '2');
INSERT INTO `rolemenu` VALUES ('489', '0', '22', '2');
INSERT INTO `rolemenu` VALUES ('490', '1', '23', '2');
INSERT INTO `rolemenu` VALUES ('491', '0', '24', '2');
INSERT INTO `rolemenu` VALUES ('492', '0', '25', '2');
INSERT INTO `rolemenu` VALUES ('493', '0', '26', '2');
INSERT INTO `rolemenu` VALUES ('494', '0', '27', '2');
INSERT INTO `rolemenu` VALUES ('495', '0', '28', '2');
INSERT INTO `rolemenu` VALUES ('496', '0', '29', '2');
INSERT INTO `rolemenu` VALUES ('497', '1', '30', '2');
INSERT INTO `rolemenu` VALUES ('623', '1', '1', '3');
INSERT INTO `rolemenu` VALUES ('624', '1', '2', '3');
INSERT INTO `rolemenu` VALUES ('625', '0', '3', '3');
INSERT INTO `rolemenu` VALUES ('626', '1', '4', '3');
INSERT INTO `rolemenu` VALUES ('627', '1', '5', '3');
INSERT INTO `rolemenu` VALUES ('628', '1', '6', '3');
INSERT INTO `rolemenu` VALUES ('629', '0', '7', '3');
INSERT INTO `rolemenu` VALUES ('630', '0', '8', '3');
INSERT INTO `rolemenu` VALUES ('631', '0', '9', '3');
INSERT INTO `rolemenu` VALUES ('632', '0', '10', '3');
INSERT INTO `rolemenu` VALUES ('633', '0', '11', '3');
INSERT INTO `rolemenu` VALUES ('634', '0', '12', '3');
INSERT INTO `rolemenu` VALUES ('635', '1', '13', '3');
INSERT INTO `rolemenu` VALUES ('636', '1', '14', '3');
INSERT INTO `rolemenu` VALUES ('637', '0', '15', '3');
INSERT INTO `rolemenu` VALUES ('638', '0', '16', '3');
INSERT INTO `rolemenu` VALUES ('639', '0', '17', '3');
INSERT INTO `rolemenu` VALUES ('640', '0', '18', '3');
INSERT INTO `rolemenu` VALUES ('641', '0', '19', '3');
INSERT INTO `rolemenu` VALUES ('642', '0', '20', '3');
INSERT INTO `rolemenu` VALUES ('643', '0', '21', '3');
INSERT INTO `rolemenu` VALUES ('644', '0', '22', '3');
INSERT INTO `rolemenu` VALUES ('645', '1', '23', '3');
INSERT INTO `rolemenu` VALUES ('646', '0', '24', '3');
INSERT INTO `rolemenu` VALUES ('647', '0', '25', '3');
INSERT INTO `rolemenu` VALUES ('648', '0', '26', '3');
INSERT INTO `rolemenu` VALUES ('649', '0', '27', '3');
INSERT INTO `rolemenu` VALUES ('650', '0', '28', '3');
INSERT INTO `rolemenu` VALUES ('651', '0', '29', '3');
INSERT INTO `rolemenu` VALUES ('652', '0', '30', '3');
INSERT INTO `rolemenu` VALUES ('654', '1', '1', '1');
INSERT INTO `rolemenu` VALUES ('655', '1', '2', '1');
INSERT INTO `rolemenu` VALUES ('656', '1', '3', '1');
INSERT INTO `rolemenu` VALUES ('657', '1', '4', '1');
INSERT INTO `rolemenu` VALUES ('658', '1', '5', '1');
INSERT INTO `rolemenu` VALUES ('659', '1', '6', '1');
INSERT INTO `rolemenu` VALUES ('660', '1', '7', '1');
INSERT INTO `rolemenu` VALUES ('661', '1', '8', '1');
INSERT INTO `rolemenu` VALUES ('662', '1', '9', '1');
INSERT INTO `rolemenu` VALUES ('663', '1', '10', '1');
INSERT INTO `rolemenu` VALUES ('664', '1', '11', '1');
INSERT INTO `rolemenu` VALUES ('665', '1', '12', '1');
INSERT INTO `rolemenu` VALUES ('666', '1', '13', '1');
INSERT INTO `rolemenu` VALUES ('667', '1', '14', '1');
INSERT INTO `rolemenu` VALUES ('668', '1', '15', '1');
INSERT INTO `rolemenu` VALUES ('669', '1', '16', '1');
INSERT INTO `rolemenu` VALUES ('670', '1', '17', '1');
INSERT INTO `rolemenu` VALUES ('671', '1', '18', '1');
INSERT INTO `rolemenu` VALUES ('672', '1', '19', '1');
INSERT INTO `rolemenu` VALUES ('673', '1', '20', '1');
INSERT INTO `rolemenu` VALUES ('674', '1', '21', '1');
INSERT INTO `rolemenu` VALUES ('675', '1', '22', '1');
INSERT INTO `rolemenu` VALUES ('676', '1', '23', '1');
INSERT INTO `rolemenu` VALUES ('677', '1', '24', '1');
INSERT INTO `rolemenu` VALUES ('678', '1', '25', '1');
INSERT INTO `rolemenu` VALUES ('679', '1', '26', '1');
INSERT INTO `rolemenu` VALUES ('680', '1', '27', '1');
INSERT INTO `rolemenu` VALUES ('681', '1', '28', '1');
INSERT INTO `rolemenu` VALUES ('682', '1', '29', '1');
INSERT INTO `rolemenu` VALUES ('683', '1', '30', '1');

-- ----------------------------
-- Table structure for `rolepower`
-- ----------------------------
DROP TABLE IF EXISTS `rolepower`;
CREATE TABLE `rolepower` (
  `Id` int(11) NOT NULL AUTO_INCREMENT,
  `RoleId` int(11) NOT NULL,
  `PowerId` int(11) NOT NULL,
  PRIMARY KEY (`Id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of rolepower
-- ----------------------------

-- ----------------------------
-- Table structure for `users`
-- ----------------------------
DROP TABLE IF EXISTS `users`;
CREATE TABLE `users` (
  `Id` int(11) NOT NULL AUTO_INCREMENT,
  `Createtime` datetime(6) NOT NULL,
  `Disable` int(11) NOT NULL,
  `Disabledesc` longtext,
  `Login_name` longtext,
  `Role_id` int(11) NOT NULL,
  `User_email` longtext,
  `User_gender` int(11) NOT NULL,
  `User_ip` longtext,
  `User_name` longtext,
  `User_pwd` longtext,
  PRIMARY KEY (`Id`),
  KEY `roleid` (`Role_id`),
  CONSTRAINT `roleid` FOREIGN KEY (`Role_id`) REFERENCES `role` (`Id`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of users
-- ----------------------------
INSERT INTO `users` VALUES ('1', '2017-04-05 19:33:55.218509', '1', null, 'visitor', '3', '1@qq.com', '0', null, '游客', '_a2407dcd');
INSERT INTO `users` VALUES ('2', '2017-03-23 12:15:31.020187', '1', null, 'admin', '1', '14124@qq.com', '0', null, '超级管理员', '_a2407dcd');
INSERT INTO `users` VALUES ('3', '2017-03-23 09:09:27.513679', '1', null, '1234', '2', '1111@qq.com', '0', null, '员工1', '_a2407dcd');
INSERT INTO `users` VALUES ('6', '2017-03-31 14:07:00.342987', '1', null, 'szcs890621', '1', '396873675@qq.com', '0', null, '陆雄斌', '_42977a93');
INSERT INTO `users` VALUES ('7', '2017-05-02 13:06:26.905697', '1', '测试用户', 'testtest', '3', '123123@qq.com', '0', null, '测试', '_42977a93');

-- ----------------------------
-- Table structure for `__efmigrationshistory`
-- ----------------------------
DROP TABLE IF EXISTS `__efmigrationshistory`;
CREATE TABLE `__efmigrationshistory` (
  `MigrationId` varchar(95) NOT NULL,
  `ProductVersion` varchar(32) NOT NULL,
  PRIMARY KEY (`MigrationId`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of __efmigrationshistory
-- ----------------------------
INSERT INTO `__efmigrationshistory` VALUES ('20170314050922_tert6666', '1.1.1');
INSERT INTO `__efmigrationshistory` VALUES ('20170316015701_JFNETCORE316', '1.1.1');
INSERT INTO `__efmigrationshistory` VALUES ('20170321054328_JFNETCORE', '1.1.1');
INSERT INTO `__efmigrationshistory` VALUES ('20170323043723_JFNETCORE3', '1.1.1');
INSERT INTO `__efmigrationshistory` VALUES ('20170324024449_JFNETCORE324', '1.1.1');
INSERT INTO `__efmigrationshistory` VALUES ('20170325001802_JFNETCORE25', '1.1.1');
INSERT INTO `__efmigrationshistory` VALUES ('20170328025746_JFNETCORE328', '1.1.1');
INSERT INTO `__efmigrationshistory` VALUES ('20170328064833_JFNETCORE3281', '1.1.1');
INSERT INTO `__efmigrationshistory` VALUES ('20170330015544_JFNETCORE330', '1.1.1');
INSERT INTO `__efmigrationshistory` VALUES ('20170330015644_JFNETCORE3301', '1.1.1');

-- ----------------------------
-- View structure for `varticle`
-- ----------------------------
DROP VIEW IF EXISTS `varticle`;
CREATE ALGORITHM=UNDEFINED DEFINER=`jinfan`@`%` SQL SECURITY DEFINER VIEW `varticle` AS select `a`.`Id` AS `Id`,`a`.`ArticleType_Id` AS `ArticleType_Id`,`a`.`Article_key` AS `Article_key`,`a`.`Article_name` AS `Article_name`,`a`.`Article_txt` AS `Article_txt`,`a`.`Createtime` AS `Createtime`,`a`.`Disable` AS `Disable`,`a`.`Disabledesc` AS `Disabledesc`,`a`.`User_Id` AS `User_Id`,`a`.`Admin_Createtime` AS `Admin_Createtime`,`a`.`Admin_Id` AS `Admin_Id`,`a`.`Admin_remark` AS `Admin_remark`,`a`.`Admin_step` AS `Admin_step`,`b`.`ArticleType_name` AS `ArticleType_name`,`c`.`User_name` AS `User_name` from ((`article` `a` join `articletype` `b` on((`a`.`ArticleType_Id` = `b`.`Id`))) join `users` `c` on((`a`.`User_Id` = `c`.`Id`))) ;

-- ----------------------------
-- View structure for `vlog`
-- ----------------------------
DROP VIEW IF EXISTS `vlog`;
CREATE ALGORITHM=UNDEFINED DEFINER=`jinfan`@`%` SQL SECURITY DEFINER VIEW `vlog` AS select `a`.`Id` AS `Id`,`a`.`Createtime` AS `Createtime`,`a`.`Log_ip` AS `Log_ip`,`a`.`Log_operation` AS `Log_operation`,`a`.`Log_remark` AS `Log_remark`,`a`.`Log_userid` AS `Log_userid`,`b`.`Login_name` AS `Login_name`,`b`.`User_name` AS `User_name`,`c`.`Role_name` AS `Role_name`,`d`.`Operation` AS `Operation` from (((`log` `a` join `users` `b` on((`a`.`Log_userid` = `b`.`Id`))) join `role` `c` on((`b`.`Role_id` = `c`.`Id`))) join `operation` `d` on((`a`.`Log_operation` = `d`.`Id`))) ;

-- ----------------------------
-- View structure for `vmenu`
-- ----------------------------
DROP VIEW IF EXISTS `vmenu`;
CREATE ALGORITHM=UNDEFINED DEFINER=`jinfan`@`%` SQL SECURITY DEFINER VIEW `vmenu` AS select `a`.`Id` AS `Id`,`a`.`Createtime` AS `Createtime`,`a`.`Menu_idname` AS `Menu_idname`,`a`.`Menu_name` AS `Menu_name`,`a`.`Menu_rank` AS `Menu_rank`,`a`.`Menu_target` AS `Menu_target`,`a`.`Menu_url` AS `Menu_url`,`a`.`Parent_Id` AS `Parent_Id`,`b`.`Role_Id` AS `Role_Id`,`c`.`Role_name` AS `Role_name`,`b`.`Disable` AS `Disable`,`a`.`Disabledesc` AS `Disabledesc`,`a`.`Disable` AS `MenuDisable` from ((`menu` `a` join `rolemenu` `b` on((`a`.`Id` = `b`.`Menu_Id`))) join `role` `c` on((`c`.`Id` = `b`.`Role_Id`))) ;

-- ----------------------------
-- View structure for `vproduct`
-- ----------------------------
DROP VIEW IF EXISTS `vproduct`;
CREATE ALGORITHM=UNDEFINED DEFINER=`jinfan`@`%` SQL SECURITY DEFINER VIEW `vproduct` AS select `a`.`Id` AS `Id`,`a`.`Createtime` AS `Createtime`,`a`.`Disable` AS `Disable`,`a`.`Disabledesc` AS `Disabledesc`,`a`.`ProductType_Id` AS `ProductType_Id`,`a`.`Product_No` AS `Product_No`,`a`.`Product_name` AS `Product_name`,`a`.`Product_txt` AS `Product_txt`,`a`.`Product_url` AS `Product_url`,`a`.`User_Id` AS `User_Id`,`b`.`ProductType_name` AS `ProductType_name`,`b`.`ProductType_No` AS `ProductType_No`,`b`.`ProductType_txt` AS `ProductType_txt` from (`product` `a` join `producttype` `b` on((`a`.`ProductType_Id` = `b`.`Id`))) ;

-- ----------------------------
-- View structure for `vreply`
-- ----------------------------
DROP VIEW IF EXISTS `vreply`;
CREATE ALGORITHM=UNDEFINED DEFINER=`jinfan`@`%` SQL SECURITY DEFINER VIEW `vreply` AS select `a`.`Id` AS `Id`,`a`.`Article_Id` AS `Article_Id`,`a`.`Createtime` AS `Createtime`,`a`.`Parent_Id` AS `Parent_Id`,`a`.`Reply_txt` AS `Reply_txt`,`a`.`User_Id` AS `User_Id`,`b`.`User_name` AS `User_name`,`c`.`Article_name` AS `Article_name`,`c`.`ArticleType_Id` AS `ArticleType_Id`,`d`.`ArticleType_name` AS `ArticleType_name` from (((`reply` `a` join `users` `b` on((`a`.`User_Id` = `b`.`Id`))) join `article` `c` on((`a`.`Article_Id` = `c`.`Id`))) join `articletype` `d` on((`c`.`ArticleType_Id` = `d`.`Id`))) ;

-- ----------------------------
-- View structure for `vusers`
-- ----------------------------
DROP VIEW IF EXISTS `vusers`;
CREATE ALGORITHM=UNDEFINED DEFINER=`jinfan`@`%` SQL SECURITY DEFINER VIEW `vusers` AS select `a`.`Id` AS `Id`,`a`.`User_name` AS `User_name`,`a`.`Role_id` AS `Role_id`,`b`.`Role_name` AS `Role_name`,`a`.`Login_name` AS `Login_name`,`a`.`User_pwd` AS `User_pwd`,`a`.`User_email` AS `User_email`,`a`.`User_gender` AS `User_gender`,`a`.`User_ip` AS `User_ip`,`a`.`Createtime` AS `Createtime`,`a`.`Disabledesc` AS `Disabledesc`,`a`.`Disable` AS `Disable` from (`users` `a` join `role` `b` on((`a`.`Role_id` = `b`.`Id`))) ;
