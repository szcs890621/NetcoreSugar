﻿using System;
using System.Linq;
using System.Text;

namespace Models
{
    public class article
    {
        
        /// <summary>
        /// Desc:- 
        /// Default:- 
        /// Nullable:False 
        /// </summary>
        public int Id {get;set;}

        /// <summary>
        /// Desc:- 
        /// Default:- 
        /// Nullable:False 
        /// </summary>
        public int ArticleType_Id {get;set;}

        /// <summary>
        /// Desc:- 
        /// Default:- 
        /// Nullable:True 
        /// </summary>
        public string Article_key {get;set;}

        /// <summary>
        /// Desc:- 
        /// Default:- 
        /// Nullable:True 
        /// </summary>
        public string Article_name {get;set;}

        /// <summary>
        /// Desc:- 
        /// Default:- 
        /// Nullable:True 
        /// </summary>
        public string Article_txt {get;set;}

        /// <summary>
        /// Desc:- 
        /// Default:- 
        /// Nullable:False 
        /// </summary>
        public DateTime Createtime {get;set;}

        /// <summary>
        /// Desc:- 
        /// Default:- 
        /// Nullable:False 
        /// </summary>
        public int Disable {get;set;}

        /// <summary>
        /// Desc:- 
        /// Default:- 
        /// Nullable:True 
        /// </summary>
        public string Disabledesc {get;set;}

        /// <summary>
        /// Desc:- 
        /// Default:- 
        /// Nullable:False 
        /// </summary>
        public int User_Id {get;set;}

        /// <summary>
        /// Desc:- 
        /// Default:0001-01-01 00:00:00.000000 
        /// Nullable:False 
        /// </summary>
        public DateTime Admin_Createtime {get;set;}

        /// <summary>
        /// Desc:- 
        /// Default:0 
        /// Nullable:False 
        /// </summary>
        public int Admin_Id {get;set;}

        /// <summary>
        /// Desc:- 
        /// Default:- 
        /// Nullable:True 
        /// </summary>
        public string Admin_remark {get;set;}

        /// <summary>
        /// Desc:- 
        /// Default:0 
        /// Nullable:False 
        /// </summary>
        public int Admin_step {get;set;}

    }
}
