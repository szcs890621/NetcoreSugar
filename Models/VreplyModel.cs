﻿using System;
using System.Linq;
using System.Text;

namespace Models
{
    public class VreplyModel
    {
        
        /// <summary>
        ///
        /// </summary>
        public int Id {get;set;}

        /// <summary>
        ///
        /// </summary>
        public int Article_Id {get;set;}

        /// <summary>
        ///
        /// </summary>
        public DateTime Createtime {get;set;}

        /// <summary>
        ///
        /// </summary>
        public int Parent_Id {get;set;}

        /// <summary>
        ///
        /// </summary>
        public string Reply_txt {get;set;}

        /// <summary>
        ///
        /// </summary>
        public int User_Id {get;set;}

        /// <summary>
        ///
        /// </summary>
        public string User_name {get;set;}

        /// <summary>
        ///
        /// </summary>
        public string Article_name {get;set;}

        /// <summary>
        ///
        /// </summary>
        public int ArticleType_Id {get;set;}

        /// <summary>
        ///
        /// </summary>
        public string ArticleType_name {get;set;}

    }
}
