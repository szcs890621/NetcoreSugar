﻿using Models;
using System;
using System.Collections.Generic;
using System.Text;

namespace Application.IService
{
   public interface ImenuService
    {
        #region 查询

        /// <summary>
        /// 查询单条根据主键
        /// </summary>
        menu GetById(int? id);

        /// <summary>
        /// 查询所有
        /// </summary>
        Queryable<menu> GetList();

        /// <summary>
        /// 根据条件查询
        /// </summary>
        Queryable<menu> GetList(Expression<Func<menu, bool>> where);

        /// <summary>
        /// 按页查询
        /// </summary>
        Queryable<menu> GetPageList(Expression<Func<menu, bool>> where, int pageIndex, int pageSize, string OrderFields);
        #endregion

        #region 删除
        /// <summary>
        /// 根据主键删除
        /// </summary>
        bool DeleteById(int id);

        /// <summary>
        /// 根据主键组批量删除
        /// </summary>
        bool DeleteById(int[] ids);

        /// <summary>
        /// 根据实体删除【一定要带主键！！！】
        /// </summary>
        bool DeleteByModel(menu model);

        #endregion

        #region 新增
        /// <summary>
        /// 强制新增，不管主键
        /// </summary>
        menu Insert(menu model);

        /// <summary>
        /// 批量插入 适合海量数据插入
        /// </summary>
        bool InsertRange(List<menu> list);

        #endregion

        #region 修改
        /// <summary>
        /// 根据实体更新【主键一定要有！！！】
        /// </summary>
        bool Update(menu model);

        /// <summary>
        /// 10条以上批量更新
        /// </summary>
        bool UpdateRange(List<menu> list);
        #endregion
    }
}
