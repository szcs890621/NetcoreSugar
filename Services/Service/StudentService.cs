﻿using System;
using System.Collections.Generic;
using System.Text;
using Models;
using Repositories;
using Application.IService;
using Repositories.IRepository;

namespace Application.Service
{
    public class menuService : ImenuService
    {

        private readonly ImenuRepository _repository;
        public menuService(ImenuRepository repository)
        {
            _repository = repository;
        }

        #region 查询

        /// <summary>
        /// 查询单条根据主键
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public menu GetById(int? id)
        {
            return _repository.GetById(id);
        }

        /// <summary>
        /// 查询所有
        /// </summary>
        /// <returns></returns>
        public Queryable<menu> GetList()
        {
            return _repository.GetList();
        }

        /// <summary>
        /// 根据条件查询
        /// </summary>
        /// <param name="where"></param>
        /// <returns></returns>
        public Queryable<menu> GetList(Expression<Func<menu, bool>> where)
        {
            return _repository.GetList(where);
        }

        /// <summary>
        /// 按页查询
        /// </summary>
        public Queryable<menu> GetPageList(Expression<Func<menu, bool>> where, int pageIndex, int pageSize, string OrderFields)
        {
            return _repository.GetPageList(where, pageIndex, pageSize, OrderFields);
        }
        #endregion

        #region 删除
        /// <summary>
        /// 根据主键删除
        /// </summary>
        public bool DeleteById(int id)
        {
            return _repository.DeleteById(id);
        }

        /// <summary>
        /// 根据主键组批量删除
        /// </summary>
        public bool DeleteById(int[] ids)
        {
            return _repository.DeleteById(ids);
        }

        /// <summary>
        /// 根据实体删除【一定要带主键！！！】
        /// </summary>
        public bool DeleteByModel(menu model)
        {
            return _repository.DeleteByModel(model);
        }

        #endregion

        #region 新增
        /// <summary>
        /// 强制新增，不管主键
        /// </summary>
        public menu Insert(menu model)
        {
            return _repository.Insert(model);
        }

        /// <summary>
        /// 批量插入 适合海量数据插入
        /// </summary>
        public bool InsertRange(List<menu> list)
        {
            return _repository.InsertRange(list);
        }

        #endregion

        #region 修改
        /// <summary>
        /// 根据实体更新【主键一定要有！！！】
        /// </summary>
        public bool Update(menu model)
        {
            return _repository.Update(model);
        }

        /// <summary>
        /// 10条以上批量更新
        /// </summary>
        public bool UpdateRange(List<menu> list)
        {
            return _repository.UpdateRange(list);
        }
        #endregion
    }
}
