﻿using Microsoft.AspNetCore.Mvc;
namespace Web.Controllers
{
    public class HomeController : Controller
    {

        //private IStudentService _studentService;
        //public HomeController(IStudentService studentService)
        //{
        //    _studentService = studentService;
        //}

        public IActionResult Index()
        {
            //Models.student model2 = _studentService.GetById(1);
            return View();
        }

        public IActionResult About()
        {
            ViewData["Message"] = "Your application description page.";

            return View();
        }

        public IActionResult Contact()
        {
            ViewData["Message"] = "Your contact page.";

            return View();
        }

        public IActionResult Error()
        {
            return View();
        }
        

    }
}
