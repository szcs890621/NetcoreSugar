﻿using MySqlSugar;
using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using System.Text;

namespace Repositories.IRepository
{
    public interface IBaseRepository
    {

    }
    public interface IBaseRepository<T>: IBaseRepository where T : class, new()
    {
        #region 查询

        /// <summary>
        /// 查询单条根据主键
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        T GetById(int id);

        /// <summary>
        /// 查询所有
        /// </summary>
        /// <returns></returns>
        Queryable<T> GetList();

        /// <summary>
        /// 根据条件查询
        /// </summary>
        /// <param name="where"></param>
        /// <returns></returns>
        Queryable<T> GetList(Expression<Func<T, bool>> where);

        /// <summary>
        /// 按页查询
        /// </summary>
        /// <param name="where">条件</param>
        /// <param name="pageIndex">页数</param>
        /// <param name="pageSize">数量</param>
        /// <param name="OrderFields">string 支持复杂【id desc,name asc】</param>
        /// <returns></returns>
        Queryable<T> GetPageList(Expression<Func<T, bool>> where, int pageIndex, int pageSize, string OrderFields);
        #endregion

        #region 删除
        /// <summary>
        /// 根据主键删除
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        bool DeleteById(int id);

        /// <summary>
        /// 根据主键组批量删除
        /// </summary>
        /// <param name="ids"></param>
        /// <returns></returns>
        bool DeleteById(int[] ids);

        /// <summary>
        /// 根据实体删除【一定要带主键！！！】
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        bool DeleteByModel(T model);

        #endregion

        #region 新增
        /// <summary>
        /// 强制新增，不管主键
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        T Insert(T model);

        /// <summary>
        /// 批量插入 适合海量数据插入
        /// </summary>
        /// <param name="list"></param>
        /// <returns></returns>
        bool InsertRange(List<T> list);

        #endregion

        #region 修改
        /// <summary>
        /// 根据实体更新【主键一定要有！！！】
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        bool Update(T model);

        bool UpdateRange(List<T> list);
        #endregion
        
    }
}
