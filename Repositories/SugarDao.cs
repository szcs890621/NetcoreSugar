﻿using Microsoft.Extensions.Configuration;
using MySqlSugar;
using System;
using System.Collections.Generic;
using System.Text;
using Utility;

namespace Repositories
{
    /// <summary>
    /// SqlSugar
    /// </summary>
    public class SugarDao
    {
        //禁止实例化
        private SugarDao()
        {

        }


        //public static string ConnectionString
        //{
        //    get
        //    {
        //        string reval = "server=localhost;Database=SqlSugarTest;Uid=root;Pwd=123456";
        //        return reval;
        //    }
        //}

        public static SqlSugarClient GetInstance()
        {
            var db = new SqlSugarClient(SiteConfig.SiteConfigStr);
            //var db = new SqlSugarClient(sqlstr);
            db.IsEnableLogEvent = true;//启用日志事件
            db.LogEventStarting = (sql, par) => { Console.WriteLine(sql + " " + par + "\r\n"); };
            return db;
        }
    }
}
