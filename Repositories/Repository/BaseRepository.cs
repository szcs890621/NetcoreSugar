﻿using MySqlSugar;
using Repositories.IRepository;
using System;
using System.Collections.Generic;
using System.Linq.Expressions;

namespace Repositories.Repository
{

    public abstract class BaseRepository<T>: IBaseRepository<T> where T : class, new()
    {
        protected readonly SqlSugarClient _db;
        public BaseRepository()
        {
            _db  = SugarDao.GetInstance();
        }

        #region 查询

        /// <summary>
        /// 查询单条根据主键
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public T GetById(int id)
        {
            return _db.Queryable<T>().InSingle(id);
        }

        /// <summary>
        /// 查询所有
        /// </summary>
        /// <returns></returns>
        public Queryable<T> GetList()
        {
            return _db.Queryable<T>();
        }

        /// <summary>
        /// 根据条件查询
        /// </summary>
        /// <param name="where"></param>
        /// <returns></returns>
        public Queryable<T> GetList(Expression<Func<T, bool>> where)
        {
            return _db.Queryable<T>().Where(where);
        }

        /// <summary>
        /// 按页查询
        /// </summary>
        /// <param name="where">条件</param>
        /// <param name="pageIndex">页数</param>
        /// <param name="pageSize">数量</param>
        /// <param name="OrderFields">string 支持复杂【id desc,name asc】</param>
        /// <returns></returns>
        public Queryable<T> GetPageList(Expression<Func<T, bool>> where, int pageIndex, int pageSize, string OrderFields)
        {
            return _db.Queryable<T>().Where(where).OrderBy(OrderFields).Skip((pageIndex - 1) * pageSize).Take(pageSize);
        }
        #endregion

        #region 删除
        /// <summary>
        /// 根据主键删除
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public bool DeleteById(int id)
        {
          return _db.Delete<T, int>(id);
        }

        /// <summary>
        /// 根据主键组批量删除
        /// </summary>
        /// <param name="ids"></param>
        /// <returns></returns>
        public bool DeleteById(int[] ids)
        {
            return _db.Delete<T, int>(ids);
        }

        /// <summary>
        /// 根据实体删除【一定要带主键！！！】
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        public bool DeleteByModel(T model)
        {
            return _db.Delete(model);
        }

        #endregion

        #region 新增
        /// <summary>
        /// 强制新增，不管主键
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        public T Insert(T model)
        {
            return (T)_db.Insert(model);
        }

        /// <summary>
        /// 批量插入 适合海量数据插入
        /// </summary>
        /// <param name="list"></param>
        /// <returns></returns>
        public bool InsertRange(List<T> list)
        {
            return _db.SqlBulkCopy(list);
        }

        #endregion

        #region 修改
        /// <summary>
        /// 根据实体更新【主键一定要有！！！】
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        public bool Update(T model)
        {
            return _db.Update(model);
        }

        public bool UpdateRange(List<T> list)
        {
            return _db.SqlBulkReplace(list);
        }
        #endregion
        
        
    }
}
